# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import base64
from datetime import datetime, timedelta
import io
import re
import requests
import PyPDF2
import json

from dateutil.relativedelta import relativedelta
from PIL import Image
from werkzeug import urls

from odoo import api, fields, models, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.exceptions import UserError, AccessError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import url_for
from odoo.tools import sql

_logger = logging.getLogger(__name__)


class TheStatuteAndRulesOfProcedure(models.Model):
    """ Mudci The statute and rules of procedure model.
    """
    _name = 'the.statute.and.rules.of.procedure'
    _description = "The statute and rules of procedure"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=False, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    description = fields.Text('Description', translate=True)
    dfile = fields.Binary(string='Document to download')
    dfile_name = fields.Char(string='Name of the document to download')


class HealthInsuranceSpecifications(models.Model):
    """ Mudci Health insurance specifications model.
    """
    _name = 'health.insurance.specifications'
    _description = "Health insurance specifications"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=False, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    description = fields.Text('Description', translate=True)
    dfile = fields.Binary(string='Document to download')
    dfile_name = fields.Char(string='Name of the document to download')


class TheMutualistsGuide(models.Model):
    """ Mudci the mutualists guide model.
    """
    _name = 'the.mutualists.guide'
    _description = "The mutualists guide"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=False, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    description = fields.Text('Description', translate=True)
    dfile = fields.Binary(string='Document to download')
    dfile_name = fields.Char(string='Name of the document to download')


class RulesOfProcedure(models.Model):
    """ Mudci rules of procedure model.
    """
    _name = 'rules.of.procedure'
    _description = "Rules of procedure"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=False, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    description = fields.Text('Description', translate=True)
    dfile = fields.Binary(string='Document to download')
    dfile_name = fields.Char(string='Name of the document to download')
