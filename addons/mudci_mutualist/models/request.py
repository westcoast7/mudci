# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class MudciRequest(models.Model):
    """ Mudci Request model.

    This model is used to register mudci adherent request.

    """

    _name = "mudci.request"
    _description = "MUDCI Adherents Request"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'id desc'

    name = fields.Char(string='Number', copy=False, readonly=True, store=True, tracking=True, default='/')
    subscriber_name = fields.Char(string='Subscriber name')
    members_name = fields.Char(string='Member\'s name')
    registration_number = fields.Char(string='Registration number')
    id_code = fields.Char(string='ID code')
    birthday = fields.Date(string='Birthday')
    effective_date = fields.Datetime(string='Effective date')
    family_status = fields.Selection([('member', 'Member'), ('spouses', 'Spouses'), ('child', 'Child')],
                                     string='Family State', default='spouses')
    coverage = fields.Char(string='Coverage')
    phone_number = fields.Char(string='Phone number')
    deposit_date = fields.Datetime(string='Deposit date')
    date_done = fields.Datetime(string='Validation date', copy=False)
    registration_date = fields.Datetime(string='Registration Date', default=lambda self: fields.Datetime.now())
    last_name = fields.Char(string='Last name')
    first_name = fields.Char(string='First name')
    new_registration_number = fields.Char(string='New registration number')
    new_last_name = fields.Char(string='New last name')
    new_first_name = fields.Char(string='New first name')
    reason_for_the_duplicate_request = fields.Selection([('loss', 'Loss'), ('damaged_card', 'Damaged card')],
                                                        string='Reason for the duplicate request', default='loss')
    last_name_of_the_duplicate_requester = fields.Char(string='Last name of the duplicate requester')
    first_name_of_the_duplicate_requester = fields.Char(string='First name of the duplicate requester')
    registration_number_of_the_duplicate_requester = fields.Char(
        string='Registration number of the duplicate requester')
    family_status_of_the_duplicate_requester = fields.Selection(
        [('member', 'Member'), ('spouses', 'Spouses'), ('child', 'Child')],
        string='Family status of the duplicate requester', default='spouses')
    function = fields.Char(string='Function')
    service = fields.Char(string='Service')
    direction = fields.Char('Direction')
    amount_of_the_loan = fields.Float(string='Amount of the loan', default=0.0)
    deduction_quarter = fields.Selection(
        [('first_trimester', 'First trimester'), ('second_trimester', 'Second trimester'),
         ('third_trimester', 'Third trimester'), ('fourth_trimester', 'Fourth trimester')], string='Deduction quarter')
    deduction_fund = fields.Char(string='Deduction fund')
    retirement_year = fields.Char(string='Retirement year')
    # to_count_in = fields.Char(string='To count in')
    to_count_in = fields.Selection(
        [('first_trimester', 'First trimester'), ('second_trimester', 'Second trimester'),
         ('third_trimester', 'Third trimester'), ('fourth_trimester', 'Fourth trimester')], string='Deduction quarter')
    for_the_acquisition_of = fields.Selection(
        [('home_appliance', 'Home appliance'), ('furniture_and_electricity', 'Furniture and Electricity'),
         ('general_hardware', 'General hardware')], string='For the acquisition of')
    reason = fields.Text(string='Reason')
    beneficiary_ids = fields.One2many('request.beneficiary', 'request_id', string='assigns')
    request_type = fields.Selection([('identification_health_insurance_card', 'Identification health insurance card'),
                                     ('id_photo_change_form', 'Id photo change Form'),
                                     ('registration_number_change_form', 'Registration number change form'),
                                     ('name_change_form', 'Name change form'),
                                     ('duplicate_card_request', 'Duplicate card request'),
                                     ('request_for_a_certificate_of_non_royalty',
                                      'Request for a certificate of non-royalty'),
                                     ('loan_application', 'Loan application'),
                                     ('exceptional_loan_application', 'Exceptional loan application'),
                                     ('retirement_loan_application', 'Retirement loan application'),
                                     ('central_ph6_loan_application', 'Central ph6 loan application'),
                                     ('evidence_africa_loan_application', 'Evidence africa loan application'),
                                     ('french_bookstore_loan_application', 'French bookstore loan application'),
                                     ('3k_import_export_loan_application', '3k import export loan application'),
                                     ('power_of_attorney_authorization', 'Power of attorney authorization')],
                                    string='Reuqest type')
    birth_certificate = fields.Binary(string='Birth certificate')
    birth_certificate_name = fields.Char(string='Birth certificate name')
    id_photo = fields.Binary(string='ID photo')
    id_photo_name = fields.Char('Request id photo name')
    assignment_decision_bearing_the_new_number = fields.Binary(string='Assignment decision bearing the new number')
    assignment_decision_bearing_the_new_number_name = fields.Char(string='Assignment decision bearing the new number name')
    service_report = fields.Binary(string='service report')
    service_report_name = fields.Char(string='service report name')
    cni = fields.Binary(string='Request cni')
    cni_name = fields.Char(string='Request cni name')
    act_of_change_of_name = fields.Binary(string='Act of change of name')
    act_of_change_of_name_name = fields.Char(string='Act of change of name')
    marriage_certificate_for_the_married_woman = fields.Binary(string='Marriage certificate for the married woman')
    marriage_certificate_for_the_married_woman_name = fields.Char(string='Marriage certificate for the married woman name')
    certificate_of_loss = fields.Binary(string='certificate of loss')
    certificate_of_loss_name = fields.Char(string='certificate of loss name')
    damaged_card = fields.Binary(string='damaged card')
    damaged_card_name = fields.Char(string='damaged card name')
    mudci_card = fields.Binary(string='MUDCI card')
    mudci_card_name = fields.Char(string='MUDCI card name')
    bounty_slip = fields.Binary(string='Bounty slip')
    bounty_slip_name = fields.Char(string='Bounty slip name')
    proforma_invoice = fields.Binary(string='Proforma invoice')
    proforma_invoice_name = fields.Char(string='Proforma invoice name')
    power_of_attorney_authorization = fields.Binary(string='Power of attorney authorization')
    power_of_attorney_authorization_name = fields.Char(string='Power of attorney authorization name')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('being_processed', 'Being processed'),
        ('done', 'Done'),
        ('canceled', 'Canceled')
    ], string='State', default='draft')
    user_id = fields.Many2one('res.users', string='Requested by', default=lambda self: self.env.uid)
    partner_id = fields.Many2one('res.partner', string='Mutualiste')

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        return self.write({'state': 'being_processed'})

    def action_validate(self):
        return self.write({'state': 'done', 'date_done': fields.Datetime.now()})

    def action_cancel(self):
        return self.write({'state': 'canceled'})


class Beneficiary(models.Model):
    """ Mudci Request model.

    This model is used to register request beneficiary.

    """

    _name = "request.beneficiary"
    _description = "Request beneficiary"
    _order = 'id desc'

    name = fields.Char(string='Beneficiary name')
    request_id = fields.Many2one('mudci.request', string='Request')
