# -*- coding: utf-8 -*-
{
    'name': "mudci_mutualist",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'contacts'],

    # always loaded
    'data': [
        'security/mudci_mutualist_security.xml',
        'security/ir.model.access.csv',
        'views/res_partners_views.xml',
        'views/mutualist_menu_views.xml',
        'views/res_mutualist_views.xml',
        'views/form_to_download_views.xml',
        'views/requests/identification_health_insurance_card_views.xml',
        'views/requests/id_photo_change_form_views.xml',
        'views/requests/registration_number_change_form_views.xml',
        'views/requests/name_change_form_views.xml',
        'views/requests/duplicate_card_request_views.xml',
        'views/requests/request_for_a_certificate_of_non_royalty_views.xml',
        'views/requests/loan_application_views.xml',
        'views/requests/exceptional_loan_application_views.xml',
        'views/requests/retirement_loan_application_views.xml',
        'views/requests/central_loan_application_views.xml',
        'views/requests/evidence_africa_loan_application_views.xml',
        'views/requests/french_bookstore_loan_application_views.xml',
        'views/requests/import_export_loan_application_views.xml',
        'report/identification_health_insurance_card_templates.xml',
        'report/id_photo_change_form_templates.xml',
        'report/registration_number_change_form_templates.xml',
        'report/name_change_form_templates.xml',
        'report/duplicate_card_request_templates.xml',
        'report/request_for_a_certificate_of_non_royalty_templates.xml',
        'report/loan_application_templates.xml',
        'report/exceptional_loan_application_templates.xml',
        'report/retirement_loan_application_templates.xml',
        'report/central_loan_application_templates.xml',
        'report/evidence_africa_loan_application_templates.xml',
        'report/french_bookstore_loan_application_templates.xml',
        'report/import_export_loan_application_templates.xml',
        'report/power_of_attorney_authorization_templates.xml',
        'report/request_reports.xml',
        'data/email_template_data.xml',
        'data/online_request_email_data.xml',
        'data/ir_sequence_data.xml',
        'views/requests/power_of_attorney_authorization_views.xml',
        'views/contact_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
