# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class SocialSecurityBenefitsPresentation(models.Model):
    """ Mudci Social Security benefits presentation. """

    _name = "ssb.presentation"
    _description = "MUDCI Social Security benefits presentation"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class EstablishingMudciCard(models.Model):
    """ Establishing Mudci Card. """

    _name = "ssb.establishing.mudci.card"
    _description = "Establishing Mudci Card"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class SolidarityFund(models.Model):
    """ SolidarityFund. """

    _name = "ssb.solidarity.fund"
    _description = "Solidarity Fund"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class FundForFinancingJointDevelopmentProjects(models.Model):
    """ Fund for financing joint development projects. """

    _name = "ssb.fund.for.financing.joint.development.projects"
    _description = "Fund for financing joint development projects"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class LifeInsuranceAndFuneralExpensesInsurance(models.Model):
    """ Life insurance and funeral expenses insurance. """

    _name = "ssb.life.insurance.and.funeral.expenses.insurance"
    _description = "Life insurance and funeral expenses insurance"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class MotorFleetInsurance(models.Model):
    """ Motor fleet insurance. """

    _name = "ssb.motor.fleet.insurance"
    _description = "Motor Fleet Insurance"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)
