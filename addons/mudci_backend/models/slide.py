# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import base64
from datetime import datetime, timedelta
import io
import re
import requests
import PyPDF2
import json

from dateutil.relativedelta import relativedelta
from PIL import Image
from werkzeug import urls

from odoo import api, fields, models, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.exceptions import UserError, AccessError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import url_for
from odoo.tools import sql

_logger = logging.getLogger(__name__)


class MudciSlide(models.Model):
    """ Mudci Slide model.

    This model is used to handle website slides.

    """
    _name = 'mudci.slide'
    _description = "MUDCI Website Slide"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=False, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    description = fields.Text('Description', translate=True)
    btn_name = fields.Char(string='Btn name')
    btn_link = fields.Char(string='Btn link')
    expires_at = fields.Datetime(string='Expires at')
    type = fields.Selection([('momentary', 'Momentary'), ('permanent', 'Permanent')])
    is_clickable = fields.Boolean(string='Is clickable', default=False)

    def _cron_deactivate_expired_slides(self):
        domain = [('expires_at', '<=', fields.Datetime.now())]
        for slide in self.search(domain):
            slide.active = False
