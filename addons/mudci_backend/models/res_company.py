# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from ast import literal_eval


class Company(models.Model):
    _inherit = "res.company"

    phone2 = fields.Char(string='Phone 2')
    fix = fields.Char(string='Fix number')
    phone3 = fields.Char(string='Phone 3')
    fax = fields.Char(string='Fax')
    postal_address = fields.Char(string='Postal address')
    opening_time_ids = fields.One2many('opening.time', 'company_id', string='opening times')
    useful_links_ids = fields.One2many('mudci.useful.links', 'company_id', string='Useful links')
    short_description = fields.Html('Short description')
    privacy_policy = fields.Html('Privacy Policy')


class OpeningTime(models.Model):
    _name = "opening.time"

    opening_day = fields.Selection([
        ('monday', 'monday'),
        ('tuesday', 'tuesday'),
        ('wednesday', 'wednesday'),
        ('thursday', 'thursday'),
        ('friday', 'friday'),
        ('saturday', 'saturday'),
        ('sunday', 'sunday'),
    ], string='Opening day')

    closing_day = fields.Selection([
        ('monday', 'monday'),
        ('tuesday', 'tuesday'),
        ('wednesday', 'wednesday'),
        ('thursday', 'thursday'),
        ('friday', 'friday'),
        ('saturday', 'saturday'),
        ('sunday', 'sunday'),
    ], string='Closing day')

    opening_time = fields.Float('Opening time')
    closing_time = fields.Float('Closing time')
    company_id = fields.Many2one('res.company', string='Company')
