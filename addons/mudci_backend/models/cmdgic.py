# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class CmdgicHistorical(models.Model):
    """ CMDGIC Historical. """

    _name = "cmdgic.historical"
    _description = "CMDGIC Historical"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class CmdgicPresentation(models.Model):
    """ CMDGIC Presentation. """

    _name = "cmdgic.presentation"
    _description = "CMDGIC Presentation"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class CmdgicCustomerJourney(models.Model):
    """ CMDGIC Customer Journey. """

    _name = "cmdgic.customer.journey"
    _description = "CMDGIC Customer Journey"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class CmdgicSpeciality(models.Model):
    """ CMDGIC Specialities. """

    _name = "cmdgic.speciality"
    _description = "CMDGIC Specialities"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class CmdgicProgram(models.Model):
    """ CMDGIC Program. """

    _name = "cmdgic.program"
    _description = "CMDGIC Program"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class CmdgicMedicalPartner(models.Model):
    """ CMDGIC Medical Partners. """

    _name = "cmdgic.medical.partner"
    _description = "CMDGIC Medical Partners"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _inherits = {
        'res.partner': 'partner_id',
    }
    _order = "id desc"

    partner_id = fields.Many2one('res.partner', string='Related partner', required=True, ondelete='cascade')


class TechnicalPlatform(models.Model):
    """ Technical platform. """

    _name = "cmdgic.technical.platform"
    _description = "Technical platform"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)
