# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime, timedelta
import logging

_logger = logging.getLogger(__name__)


class MudciNewsflash(models.Model):
    """ Mudci Newsflash model.

    This model is used to add Newsflash.

    """

    _name = "mudci.newsflash"
    _description = "MUDCI Newsflash"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Char(string='Description')
    active = fields.Boolean(default=True, tracking=1)
    expires_at = fields.Datetime(string='Expires at')
    link = fields.Char(string='Link')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)

    def _cron_deactivate_expired_news_flash(self):
        domain = [('expires_at', '<=', fields.Datetime.now())]
        for flash in self.search(domain):
            flash.active = False
