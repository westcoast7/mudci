# -*- coding: utf-8 -*-

import base64
import json
import logging
import requests
from datetime import datetime, timedelta
from pytz import timezone, UTC
from datetime import time

from odoo import _
from odoo import http
from odoo.http import Controller, request, route

_logger = logging.getLogger(__name__)


class ConsumeWebServices(Controller):

    @http.route('/cmdgic/programme', auth='public', website=True)
    def cmdgic_program(self, **kw):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')

        values = {
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.doctor_programs', values)

    @http.route('/programmes-medecins', auth='public', website=True)
    def doctor_programs(self, **kw):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                    order='id desc')

        values = {
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.doctor_programs', values)

    @http.route('/programme/medecin-conseil/periodes', type='http', auth='public', methods=['GET'], website=True,
                csrf=False)
    def periods_doctors_advice(self, **kw):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)],
                                                                                    limit=1, order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=periodeConseil'
        res = requests.get(url).json()

        values = {
            'periods': res,
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.periods_doctors_advice', values)

    @http.route('/periodes/<int:period_id>/medecins-conseils', type='http', auth='public', methods=['GET'],
                website=True, csrf=False)
    def periods_medecins_conseils(self, period_id):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)],
                                                                                    limit=1, order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=specialitesOfProgrammeConseil&periode=%s'
        res = requests.get(url % (period_id)).json()

        values = {
            'medecin_type': 'conseils',
            'period_id': period_id,
            'specialities': res,
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.periods_medecins', values)

    @http.route('/programme/medecin-resident/periodes', type='http', auth='public', methods=['GET'], website=True,
                csrf=False)
    def resident_doctor_program(self, **kw):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)],
                                                                                    limit=1, order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=periodeResident'
        res = requests.get(url).json()

        values = {
            'periods': res,
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.resident_doctor_program', values)

    @http.route('/periodes/<int:period_id>/medecins-residents', type='http', auth='public', methods=['GET'],
                website=True, csrf=False)
    def periods_medecins_residents(self, period_id):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)],
                                                                                    limit=1, order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=specialitesOfProgrammeResident&periode=%s'
        res = requests.get(url % (period_id)).json()

        values = {
            'medecin_type': 'residents',
            'period_id': period_id,
            'specialities': res,
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.periods_medecins', values)

    @http.route('/programme/medecin-vacataires/periodes', type='http', auth='public', methods=['GET'], website=True,
                csrf=False)
    def temporary_doctor_program(self, **kw):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)],
                                                                                    limit=1, order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=periodeVacataire'
        res = requests.get(url).json()

        values = {
            'periods': res,
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.temporary_doctor_program', values)

    @http.route('/periodes/<int:period_id>/medecins-vacataires', type='http', auth='public', methods=['GET'],
                website=True, csrf=False)
    def periods_temporary_doctor(self, period_id):
        doctor_programs_banner = request.env['doctor.program.banner'].sudo().search([('active', '=', True)],
                                                                                    limit=1, order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=specialitesOfProgrammeVacataire&periode=%s'
        res = requests.get(url % (period_id)).json()

        values = {
            'medecin_type': 'vacataires',
            'period_id': period_id,
            'specialities': res,
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.periods_medecins', values)

    @http.route('/medecin/details/', type='http', auth='public', methods=['POST'], csrf=False)
    def medecins_details(self, **post):
        periods_medecins = []

        if post and request.httprequest.method == 'POST':
            url = ''
            if post['medecin_type'] == 'conseils':
                url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=programmeOfThisConseil&periode=%s&medecin=%s'
            elif post['medecin_type'] == 'residents':
                url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=programmeOfThisResident&periode=%s&medecin=%s'
            elif post['medecin_type'] == 'vacataires':
                url = 'http://vps724169.ovh.net/admin/JSON_WEB/medecinsProgramme.php?action=programmeOfThisVacataire&periode=%s&medecin=%s'
            res = requests.get(url % (int(post['period_id']), int(post['medecin_id']))).json()

            for r in res:
                items = {
                    'jour': r.get('jour'),
                    'heureDebut': r.get('heureDebut'),
                    'heureFin': r.get('heureFin'),
                }
                periods_medecins.append(items)

        return json.dumps({'medecin_type': post['medecin_type'], 'periods_medecins': periods_medecins})

    @http.route('/cmdgic/specialites', auth='public', website=True)
    def cmdgic_specialties(self, **kw):
        cmdgic_specialties = request.env['cmdgic.speciality'].sudo().search([], order='id desc')
        cmdgic_specialties_banner = request.env['cmdgic.specialty.banner'].sudo().search([('active', '=', True)],
                                                                                         limit=1, order='id desc')
        # url = 'http://vps724169.ovh.net/admin/JSON_WEB/all_mudci_specialites_json.php'
        # res = requests.get(url).json()

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            # 'cmdgic_specialties': res['specialitesMudci'],
            'cmdgic_specialties': cmdgic_specialties,
            'cmdgic_specialties_banner': cmdgic_specialties_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_specialties', values)

    @http.route('/cmdgic/examens/medicaux', auth='public', website=True)
    def cmdgic_medical_exams(self, **kw):
        medical_exam_banner = request.env['medical.exam.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/prestation.php?action=prestation'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'cmdgic_medical_exams': res,
            'medical_exam_banner': medical_exam_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_medical_exams', values)

    @http.route('/cmdgic/examen/<int:examen_id>/<string:examen_name>/services', type='http', auth='public',
                methods=['GET'], website=True, csrf=False)
    def cmdgic_examen_services(self, examen_id, examen_name):
        medical_exam_banner = request.env['medical.exam.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/prestation.php?action=sousPrestation&id=%s'
        res = requests.get(url % (examen_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'cmdgic_examen_services': res,
            'medical_exam_banner': medical_exam_banner,
            'examen_name': examen_name.capitalize(),
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.cmdgic_examen_services', values)

    @http.route('/cmdgic/centres/optiques', auth='public', website=True)
    def cmdgic_optical_centers(self, **kw):
        optical_center_banner = request.env['optical.center.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                   order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/optique.php?action=villeofAbidjan'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'optical_center_banner': optical_center_banner,
            'cmdgic_optical_centers_abidjan_city': res,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_optical_centers', values)

    @http.route('/cmdgic/centres/optiques/interieur/villes', auth='public', website=True)
    def cmdgic_interior_optical_centers_by_city(self, **kw):
        optical_center_banner = request.env['optical.center.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                   order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/optique.php?action=villeofInterieur'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'optical_center_banner': optical_center_banner,
            'cmdgic_interior_optical_centers_by_city': res,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_interior_optical_centers_by_city', values)

    @http.route('/cmdgic/centres/optiques/<string:locality>/<int:city_id>/<string:city_name>/communes', auth='public', website=True)
    def cmdgic_optical_centers_city_communes(self, locality, city_id, city_name):
        optical_center_banner = request.env['optical.center.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                   order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/optique.php?action=communesOfVille&ville=%s'
        res = requests.get(url % (city_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'optical_center_banner': optical_center_banner,
            'cmdgic_optical_centers_city_communes': res,
            'lastnews': lastnews,
            'locality': locality,
            'city_name': city_name,
            'city_id': city_id,
        }

        return http.request.render('mudci_website.cmdgic_optical_centers_city_communes', values)

    @http.route('/cmdgic/centres/optiques/<string:locality>/<int:city_id>/<string:city_name>/<int:commune_id>/<string:commune_name>/quartiers', auth='public',
                website=True)
    def cmdgic_optical_centers_commune_neighborhoods(self, locality, city_id, city_name, commune_id, commune_name):
        optical_center_banner = request.env['optical.center.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                   order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/optique.php?action=quartierOfVille&commune=%s'
        res = requests.get(url % (commune_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'optical_center_banner': optical_center_banner,
            'cmdgic_optical_centers_commune_neighborhoods': res,
            'lastnews': lastnews,
            'locality': locality,
            'city_id': city_id,
            'city_name': city_name,
            'commune_name': commune_name,
            'commune_id': commune_id,
        }

        return http.request.render('mudci_website.cmdgic_optical_centers_commune_neighborhoods', values)

    @http.route('/cmdgic/centres/optiques/<string:locality>/<int:city_id>/<string:city_name>/<int:commune_id>/<string:commune_name>/<int:piece_id>/<string:piece_name>/centres', auth='public',
                website=True)
    def cmdgic_optical_centers_piece_centers(self, locality, city_id, city_name, commune_id, commune_name, piece_id, piece_name):
        optical_center_banner = request.env['optical.center.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                   order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/optique.php?action=optiqueOfQuartier&quartier=%s'
        res = requests.get(url % (piece_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'optical_center_banner': optical_center_banner,
            'cmdgic_optical_centers_piece_centers': res,
            'lastnews': lastnews,
            'locality': locality,
            'city_id': city_id,
            'city_name': city_name,
            'commune_name': commune_name,
            'commune_id': commune_id,
            'piece_name': piece_name,
            'piece_id': piece_id,
        }

        return http.request.render('mudci_website.cmdgic_optical_centers_piece_centers', values)

    @http.route('/cmdgic/pharmacies', auth='public', website=True)
    def cmdgic_pharmacies(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_pharmacies', values)

    @http.route('/cmdgic/pharmacies/abidjan', auth='public', website=True)
    def cmdgic_pharmacies_abidjan(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_pharmacies_abidjan', values)

    @http.route('/cmdgic/pharmacies/agrees/mudci/abidjan/communes', auth='public', website=True)
    def cmdgic_municipalities_of_mudci_approved_pharmacies_abidjan(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAgreeAbidjan'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_municipalities_of_mudci_approved_pharmacies_abidjan': res,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_municipalities_of_mudci_approved_pharmacies_abidjan', values)

    @http.route('/cmdgic/pharmacies/agrees/mudci/abidjan/commune/<int:commune_id>/<string:commune_name>/quartiers',
                auth='public', website=True)
    def cmdgic_mudci_approved_pharmacies_in_abidjan_neighborhoods_by_municipalities(self, commune_id, commune_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAgreeAbidjanQuartier&commune=%s'
        res = requests.get(url % (commune_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_mudci_approved_pharmacies_in_abidjan_neighborhoods_by_municipalities': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render(
            'mudci_website.cmdgic_mudci_approved_pharmacies_in_abidjan_neighborhoods_by_municipalities', values)

    @http.route(
        '/cmdgic/pharmacies/agrees/mudci/abidjan/<int:commune_id>/<string:commune_name>/<int:neighborhoods_id>/<string:neighborhoods_name>/pharmacies',
        auth='public', website=True)
    def cmdgic_mudci_approved_pharmacies_in_abidjan_pharmacies_by_district(self, commune_id, commune_name, neighborhoods_id, neighborhoods_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAgreeAbidjanQuartierPharmacies&quartier=%s'
        res = requests.get(url % (neighborhoods_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_mudci_approved_pharmacies_in_abidjan_pharmacies_by_district': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
            'neighborhoods_id': neighborhoods_id,
            'neighborhoods_name': neighborhoods_name,
        }

        return http.request.render('mudci_website.cmdgic_mudci_approved_pharmacies_in_abidjan_pharmacies_by_district',
                                   values)

    @http.route('/cmdgic/pharmacies/abidjan/communes', auth='public', website=True)
    def cmdgic_municipalities_with_pharmacies_abidjan(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAbidjan'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_municipalities_with_pharmacies_abidjan': res,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_municipalities_with_pharmacies_abidjan', values)

    @http.route('/cmdgic/pharmacies/abidjan/commune/<int:commune_id>/<string:commune_name>/quartiers', auth='public',
                website=True)
    def cmdgic_districts_by_municipality_with_pharmacies_abidjan(self, commune_id, commune_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAbidjanQuartier&commune=%s'
        res = requests.get(url % (commune_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_districts_by_municipality_with_pharmacies_abidjan': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render('mudci_website.cmdgic_districts_by_municipality_with_pharmacies_abidjan', values)

    @http.route('/cmdgic/pharmacies/abidjan/<int:commune_id>/<string:commune_name>/<int:district_id>/<string:district_name>/pharmacies',
                auth='public', website=True)
    def cmdgic_pharmacies_by_district_with_pharmacies_abidjan(self, commune_id, commune_name, district_id, district_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAbidjanQuartierPharmacies&quartier=%s'
        res = requests.get(url % (district_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_pharmacies_by_district_with_pharmacies_abidjan': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render('mudci_website.cmdgic_pharmacies_by_district_with_pharmacies_abidjan', values)

    @http.route('/cmdgic/pharmacies/interieur', auth='public', website=True)
    def cmdgic_mudci_approved_interior_pharmacies(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_mudci_approved_interior_pharmacies', values)

    @http.route('/cmdgic/pharmacies/agrees/interieur/communes', auth='public', website=True)
    def cmdgic_municipalities_with_mudci_approved_interior_pharmacies(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAgreeInterieur'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_municipalities_with_mudci_approved_interior_pharmacies': res,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_municipalities_with_mudci_approved_interior_pharmacies',
                                   values)

    @http.route('/cmdgic/pharmacies/agrees/interieur/commune/<int:commune_id>/<string:commune_name>/quartiers',
                auth='public', website=True)
    def cmdgic_neighborhoods_by_municipality_with_mudci_approved_interior_pharmacies(self, commune_id, commune_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAgreeInterieurQuartier&commune=%s'
        res = requests.get(url % (commune_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_neighborhoods_by_municipality_with_mudci_approved_interior_pharmacies': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render(
            'mudci_website.cmdgic_neighborhoods_by_municipality_with_mudci_approved_interior_pharmacies', values)

    @http.route('/cmdgic/pharmacies/agrees/interieur/<int:commune_id>/<string:commune_name>/<int:district_id>/<string:district_name>/pharmacies',
                auth='public', website=True)
    def cmdgic_pharmacies_by_neighborhood_with_mudci_approved_interior_pharmacies(self, commune_id, commune_name, district_id, district_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeAgreeInterieurQuartierPharmacies&quartier=%s'
        res = requests.get(url % (district_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_pharmacies_by_neighborhood_with_mudci_approved_interior_pharmacies': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render(
            'mudci_website.cmdgic_pharmacies_by_neighborhood_with_mudci_approved_interior_pharmacies', values)

    @http.route('/cmdgic/pharmacies/interieur/communes', auth='public', website=True)
    def cmdgic_municipalities_with_pharmacies_in_the_interior_of_the_country(self, **kw):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeInterieur'
        res = requests.get(url).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_municipalities_with_pharmacies_in_the_interior_of_the_country': res,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_municipalities_with_pharmacies_in_the_interior_of_the_country',
                                   values)

    @http.route('/cmdgic/pharmacies/interieur/commune/<int:commune_id>/<string:commune_name>/quartiers', auth='public',
                website=True)
    def cmdgic_districts_by_municipality_with_pharmacies_in_the_interior_of_the_country(self, commune_id, commune_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeInterieurQuartier&commune=%s'
        res = requests.get(url % (commune_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_districts_by_municipality_with_pharmacies_in_the_interior_of_the_country': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render(
            'mudci_website.cmdgic_districts_by_municipality_with_pharmacies_in_the_interior_of_the_country', values)

    @http.route('/cmdgic/pharmacies/interieur/<int:commune_id>/<string:commune_name>/<int:district_id>/<string:district_name>/pharmacies',
                auth='public', website=True)
    def cmdgic_pharmacies_by_district_within_the_country(self, commune_id, commune_name, district_id, district_name):
        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        url = 'http://vps724169.ovh.net/admin/JSON_WEB/pharmacie.php?action=communeInterieurQuartierPharmacies&quartier=%s'
        res = requests.get(url % (district_id)).json()
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'cmdgic_pharmacies_by_district_within_the_country': res,
            'lastnews': lastnews,
            'commune_id': commune_id,
            'commune_name': commune_name,
        }

        return http.request.render('mudci_website.cmdgic_pharmacies_by_district_within_the_country', values)

    @http.route('/cmdgic/reseau-de-soins', auth='public', website=True)
    def cmdgic_care_network(self):
        care_network_banner = request.env['care.network.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'care_network_banner': care_network_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_care_network', values)

    @http.route('/cmdgic/reseau-de-soins/<string:locality>/type', auth='public', website=True)
    def cmdgic_care_network_type_by_locality(self, locality):

        res = None
        if locality:
            if locality == 'abidjan':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/centreSante.php?action=typeOfAbidjan'
                res = requests.get(url).json()

            if locality == 'interieur':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/centreSante.php?action=typeOfInterieur'
                res = requests.get(url).json()

        care_network_banner = request.env['care.network.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'care_network_banner': care_network_banner,
            'care_network_type_by_locality': res,
            'lastnews': lastnews,
            'locality': locality,
        }

        return http.request.render('mudci_website.cmdgic_care_network_type_by_locality', values)

    @http.route('/cmdgic/reseau-de-soins/<string:locality>/<int:type_id>/<string:type_name>/commune', auth='public', website=True)
    def cmdgic_care_township_by_locality_according_to_type(self, locality, type_id, type_name):

        res = None
        if locality:
            if locality == 'abidjan':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/centreSante.php?action=communeOfAbidjan&type=%s'
                res = requests.get(url % type_id).json()

            if locality == 'interieur':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/centreSante.php?action=communeOfInterieur&type=%s'
                res = requests.get(url % type_id).json()

        care_network_banner = request.env['care.network.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'care_network_banner': care_network_banner,
            'care_township_by_locality_according_to_type': res,
            'lastnews': lastnews,
            'locality': locality,
            'type_id': type_id,
            'type_name': type_name,
        }

        return http.request.render('mudci_website.care_township_by_locality_according_to_type', values)

    @http.route('/cmdgic/reseau-de-soins/<string:locality>/<int:type_id>/<string:type_name>/<int:township_id>/<string:township_name>/quartiers', auth='public', website=True)
    def cmdgic_care_district_by_townships_according_to_the_type(self, locality, type_id, township_id, type_name, township_name):

        res = None
        if type_id and township_id:
            url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/centreSante.php?action=quartierOfCommune&type=%s&commune=%s'
            res = requests.get(url % (type_id, township_id)).json()

        care_network_banner = request.env['care.network.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'care_network_banner': care_network_banner,
            'cmdgic_care_district_by_townships_according_to_the_type': res,
            'lastnews': lastnews,
            'type_id': type_id,
            'type_name': type_name,
            'township_name': township_name,
            'locality': locality,
            'township_id': township_id,
        }

        return http.request.render('mudci_website.cmdgic_care_district_by_townships_according_to_the_type', values)

    @http.route('/cmdgic/reseau-de-soins/<string:locality>/<int:type_id>/<string:type_name>/<int:township_id>/<string:township_name>/<int:district_id>/<string:district_name>', auth='public',
                website=True)
    def cmdgic_care_center_by_district_according_to_the_type_and_township(self, locality, type_id, township_id, district_id, type_name, township_name, district_name):

        res = None
        if type_id and township_id:
            url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/centreSante.php?action=centreOfQuartier&type=%s&commune=%s&quartier=%s'
            res = requests.get(url % (type_id, township_id, district_id)).json()

        care_network_banner = request.env['care.network.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                               order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'care_network_banner': care_network_banner,
            'cmdgic_care_center_by_district_according_to_the_type_and_township': res,
            'lastnews': lastnews,
            'type_id': type_id,
            'type_name': type_name,
            'township_id': township_id,
            'township_name': township_name,
            'district_name': district_name,
            'locality': locality,
        }

        return http.request.render('mudci_website.cmdgic_care_center_by_district_according_to_the_type_and_township', values)

    @http.route('/cmdgic/<string:locality>/pharmacies-de-garde-agrees/communes', auth='public', website=True)
    def cmdgic_municipalities_with_approved_on_duty_pharmacies_by_locality(self, locality):
        
        res = None
        if locality:
            if locality == 'abidjan':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardeCommuneAgreeAbidjan'
                res = requests.get(url).json()

            if locality == 'interieur':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardeCommuneAgreeInterieur'
                res = requests.get(url).json()

        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'municipalities_with_approved_on_duty_pharmacies_by_locality': res,
            'lastnews': lastnews,
            'locality': locality,
        }

        return http.request.render('mudci_website.municipalities_with_approved_on_duty_pharmacies_by_locality', values)

    @http.route('/cmdgic/<string:locality>/<string:commune_name>/<int:commune_id>/pharmacies-de-garde-agrees/quartiers', auth='public', website=True)
    def cmdgic_neighborhoods_of_on_duty_pharmacies_approved_by_municipality(self, locality, commune_name, commune_id):
        res = None
        if commune_id:
            url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardeQuartierAgree&commune=%s'
            res = requests.get(url % commune_id).json()

        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'neighborhoods_of_on_duty_pharmacies_approved_by_municipality': res,
            'lastnews': lastnews,
            'locality': locality,
            'commune_name': commune_name,
            'commune_id': commune_id,
        }

        return http.request.render('mudci_website.neighborhoods_of_on_duty_pharmacies_approved_by_municipality', values)

    @http.route('/cmdgic/<string:locality>/<string:commune_name>/<int:commune_id>/<string:quartier_name>/<int:quartier_id>/pharmacies-de-garde-agrees/pharmacies', auth='public', website=True)
    def cmdgic_duty_pharmacies_approved_by_district_according_to_the_municipality(self, locality, commune_name, commune_id, quartier_name, quartier_id):
        res = None
        if quartier_id:
            url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardePharmacieAgree&quartier=%s'
            res = requests.get(url % quartier_id).json()

        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'duty_pharmacies_approved_by_district_according_to_the_municipality': res,
            'lastnews': lastnews,
            'locality': locality,
            'commune_name': commune_name,
            'commune_id': commune_id,
            'quartier_name': quartier_name,
            'quartier_id': quartier_id,
        }

        return http.request.render('mudci_website.duty_pharmacies_approved_by_district_according_to_the_municipality', values)

    @http.route('/cmdgic/<string:locality>/pharmacies-de-garde/communes', auth='public', website=True)
    def cmdgic_communes_with_on_duty_pharmacies_by_locality(self, locality):

        res = None
        if locality:
            if locality == 'abidjan':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardeCommuneAbidjan'
                res = requests.get(url).json()

            if locality == 'interieur':
                url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardeCommuneInterieur'
                res = requests.get(url).json()

        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'communes_with_on_duty_pharmacies_by_locality': res,
            'lastnews': lastnews,
            'locality': locality,
        }

        return http.request.render('mudci_website.communes_with_on_duty_pharmacies_by_locality', values)

    @http.route('/cmdgic/<string:locality>/<string:commune_name>/<int:commune_id>/pharmacies-de-garde/quartiers', auth='public', website=True)
    def cmdgic_neighborhoods_of_on_duty_pharmacies_by_municipality(self, locality, commune_name, commune_id):
        res = None
        if commune_id:
            url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardeQuartier&commune=%s'
            res = requests.get(url % commune_id).json()

        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'neighborhoods_of_on_duty_pharmacies_by_municipality': res,
            'lastnews': lastnews,
            'locality': locality,
            'commune_name': commune_name,
            'commune_id': commune_id,
        }

        return http.request.render('mudci_website.neighborhoods_of_on_duty_pharmacies_by_municipality', values)

    @http.route('/cmdgic/<string:locality>/<string:commune_name>/<int:commune_id>/<string:quartier_name>/<int:quartier_id>/pharmacies-de-garde/pharmacies', auth='public', website=True)
    def cmdgic_on_duty_pharmacies_by_district_according_to_the_municipalities(self, locality, commune_name, commune_id, quartier_name, quartier_id):
        res = None
        if quartier_id:
            url = 'http://vps724169.ovh.net/admin/JSON_NEW_VERSION/pharmacie.php?action=GardePharmacie&quartier=%s'
            res = requests.get(url % quartier_id).json()

        pharmacy_banner = request.env['pharmacy.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'pharmacy_banner': pharmacy_banner,
            'on_duty_pharmacies_by_district_according_to_the_municipalities': res,
            'lastnews': lastnews,
            'locality': locality,
            'commune_name': commune_name,
            'commune_id': commune_id,
            'quartier_name': quartier_name,
            'quartier_id': quartier_id,
        }

        return http.request.render('mudci_website.on_duty_pharmacies_by_district_according_to_the_municipalities', values)
