# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import base64
from datetime import datetime, timedelta
import io
import re
import requests
import PyPDF2
import json

from dateutil.relativedelta import relativedelta
from PIL import Image
from werkzeug import urls

from odoo import api, fields, models, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.exceptions import UserError, AccessError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import url_for
from odoo.tools import sql

_logger = logging.getLogger(__name__)


class MudciContact(models.Model):
    """ Mudci Contact model.

    This model is used to handle website Contact.

    """
    _name = 'mudci.contact'
    _description = "MUDCI Website Contact"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name', required=True)
    email = fields.Char(string='Email', required=True)
    phone = fields.Char(string='Phone number')
    user_id = fields.Many2one('res.users', string='User', default=lambda self: self.env.user)
    website = fields.Char(string='Website')
    subject = fields.Char(string='Subject')
    message = fields.Text(string='Message')
