# -*- coding: utf-8 -*-

from . import res_mutualist
from . import res_partner
from . import ir_attachment
from . import request
from . import online_request
from . import form_to_download
