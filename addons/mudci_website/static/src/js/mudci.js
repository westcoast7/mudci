$(document).ready(function () {
    /*-------------------------------------
    OwlCarousel
-------------------------------------*/
    $('.rs-carousel').each(function () {
        var owlCarousel = $(this),
            loop = owlCarousel.data('loop'),
            items = owlCarousel.data('items'),
            dotsEach = owlCarousel.data('doteach'),
            margin = owlCarousel.data('margin'),
            stagePadding = owlCarousel.data('stage-padding'),
            autoplay = owlCarousel.data('autoplay'),
            autoplayTimeout = owlCarousel.data('autoplay-timeout'),
            smartSpeed = owlCarousel.data('smart-speed'),
            dots = owlCarousel.data('dots'),
            nav = owlCarousel.data('nav'),
            navSpeed = owlCarousel.data('nav-speed'),
            xsDevice = owlCarousel.data('mobile-device'),
            xsDeviceNav = owlCarousel.data('mobile-device-nav'),
            xsDeviceDots = owlCarousel.data('mobile-device-dots'),
            smDevice = owlCarousel.data('ipad-device'),
            smDeviceNav = owlCarousel.data('ipad-device-nav'),
            smDeviceDots = owlCarousel.data('ipad-device-dots'),
            smDevice2 = owlCarousel.data('ipad-device2'),
            smDeviceNav2 = owlCarousel.data('ipad-device-nav2'),
            smDeviceDots2 = owlCarousel.data('ipad-device-dots2'),
            mdDevice = owlCarousel.data('md-device'),
            lgDevice = owlCarousel.data('lg-device'),
            centerMode = owlCarousel.data('center-mode'),
            HoverPause = owlCarousel.data('hoverpause'),
            mdDeviceNav = owlCarousel.data('md-device-nav'),
            mdDeviceDots = owlCarousel.data('md-device-dots');

        owlCarousel.owlCarousel({
            loop: (loop ? true : false),
            dotsEach: (dotsEach ? true : false),
            items: (items ? items : 4),
            lazyLoad: true,
            center: (centerMode ? true : false),
            autoplayHoverPause: (HoverPause ? true : false),
            margin: (margin ? margin : 0),
            //stagePadding: (stagePadding ? stagePadding : 0),
            autoplay: (autoplay ? true : false),
            autoplayTimeout: (autoplayTimeout ? autoplayTimeout : 1000),
            smartSpeed: (smartSpeed ? smartSpeed : 250),
            dots: (dots ? true : false),
            nav: (nav ? true : false),
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            navSpeed: (navSpeed ? true : false),
            responsiveClass: true,
            responsive: {
                0: {
                    items: (xsDevice ? xsDevice : 1),
                    nav: (xsDeviceNav ? true : false),
                    dots: (xsDeviceDots ? true : false),
                    center: false,
                },
                576: {
                    items: (smDevice2 ? smDevice2 : 2),
                    nav: (smDeviceNav2 ? true : false),
                    dots: (smDeviceDots2 ? true : false),
                    center: false,
                },
                768: {
                    items: (smDevice ? smDevice : 3),
                    nav: (smDeviceNav ? true : false),
                    dots: (smDeviceDots ? true : false),
                    center: false,
                },
                992: {
                    items: (mdDevice ? mdDevice : 4),
                    nav: (mdDeviceNav ? true : false),
                    dots: (mdDeviceDots ? true : false),
                },
                1200: {
                    items: (lgDevice ? lgDevice : 4),
                }
            }
        });
    });

    // sticky menu
    var header = $('.menu-sticky');
    var win = $(window);

    win.on('scroll', function () {
        var scroll = win.scrollTop();
        if (scroll < 1) {
            header.removeClass("sticky");
        } else {
            header.addClass("sticky");
        }

        $("section").each(function () {
            var elementTop = $(this).offset().top - $('#rs-header').outerHeight();
            if (scroll >= elementTop) {
                $(this).addClass('loaded');
            }
        });

    });

    // Sticky Sidebar

    if ($('#sticky-sidebar').length) {
        var el = $('#sticky-sidebar');
        var stickyTop = $('#sticky-sidebar').offset().top;
        var stickyHeight = $('#sticky-sidebar').height();
        $(window).scroll(function () {
            var limit = $('#sticky-end').offset().top - stickyHeight - 0;
            var windowTop = $(window).scrollTop();
            if (stickyTop < windowTop) {
                el.css({
                    position: 'fixed',
                    top: -230
                });
            } else {
                el.css('position', 'static');
            }
            if (limit < windowTop) {
                var diff = limit - windowTop;
                el.css({
                    top: diff
                });
            }
        });
    }


    $("#bn1").breakingNews({
        effect: "slide-h",
        autoplay: true,
        timer: 8000,
        color: "red",
        border: true
    });


    $("#bn4").breakingNews({
        effect: "slide-v",
        autoplay: true,
        timer: 3000,
        color: 'green',
        border: true
    });


    $("#bn2").breakingNews({
        effect: "slide-h",
        autoplay: true,
        timer: 3000,
        color: "yellow"
    });


    $("#bn3").breakingNews({
        effect: "slide-v",
        autoplay: true,
        timer: 3000,
        color: "turquoise",
        border: true
    });


    $("#bn5").breakingNews({
        effect: "slide-v",
        color: 'orange'
    });


    $("#bn6").breakingNews({
        effect: "slide-h",
        autoplay: true,
        timer: 3000,
        color: 'purple'
    });


    $("#bn7").breakingNews({
        effect: "slide-v",
        autoplay: true,
        timer: 3000,
        color: 'darkred'
    });


    $("#bn8").breakingNews({
        effect: "slide-v",
        color: "black"
    });


    $("#bn9").breakingNews({
        effect: "slide-v",
        autoplay: true,
        timer: 3000,
        color: "light"
    });


    $("#bn10").breakingNews({
        effect: "slide-h",
        autoplay: true,
        timer: 3000,
        color: "pink"
    });

    // magnificPopup init
    var imagepopup = $('.image-popup');
    if(imagepopup.length){
        $('.image-popup').magnificPopup({
            type: 'image',
            callbacks: {
                beforeOpen: function() {
                   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure animated zoomInDown');
                }
            },
            gallery: {
                enabled: true
            }
        });
    }

        // Get a quote popup
    var popupquote = $('.popup-quote');
    if(popupquote.length){
        $('.popup-quote').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#qname',
            removalDelay: 500,
            callbacks: {
                beforeOpen: function() {
                    this.st.mainClass = this.st.el.attr('data-effect');
                    if(win.width() < 800) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#qname';
                    }
                }
            }
        });
    }

    //Videos popup jQuery
    var popupvideos = $('.popup-videos');
    if(popupvideos.length){
        $('.popup-videos').magnificPopup({
            disableOn: 10,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }

    // Post contact message from website homepage
    $("#id-contact-form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action');

        // disable button
        form.find("button[type='submit']").prop('disabled',true);
        // add spinner to button
        form.find("button[type='submit']").html('<i class="fa fa-circle-o-notch fa-spin"></i> loading...');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            dataType: 'json',

            success: function (response) {
                form.find("button[type='submit']").prop('disabled',false);
                form.find("button[type='submit']").html('Soumettre maintenant');
                form.trigger("reset");
                alertify.success(response);
            }
        });
    });

$('#buttonsearch').click(function(){
				$('#formsearch').slideToggle( "fast",function(){
					 $( '#content' ).toggleClass( "moremargin" );
				} );
				$('#searchbox').focus()
				$('.openclosesearch').toggle();
		});


    // Slider Custom jQuery
    var nivo_slider = $('#nivoSlider');
        if(nivo_slider.length){
        $('#nivoSlider').nivoSlider({
            effect: 'random',
            slices: 15,
            boxCols: 8,
            boxRows: 4,
            animSpeed: 500,
            pauseTime: 5000,
            startSlide: 0,
            directionNav: true,
            controlNavThumbs: false,
            pauseOnHover: true,
            manualAdvance: false
        });
    }

});

$(document).ready(function () {
    // Handler modal for justification leaves
    $('.doctorDetailModal').on('click', function () {
        var link = $(this)

        period_id = link.data('periode-id');
        medecin_id = link.data('medecin-id');
        medecin_firstname = link.data('medecin-lastname');
        medecin_lastname = link.data('medecin-firtsname');
        medecin_type = link.data('medecin-type');

        $.ajax({
            url: '/medecin/details/',
            type: 'post',
            data: {period_id: period_id, medecin_id: medecin_id, medecin_type: medecin_type},
            dataType: 'json',

            success: function (response) {
                $("#doctor-program-modal-header").html('Programme Docteur' + ' ' + medecin_lastname + ' ' + medecin_firstname);
                var len = response['periods_medecins'].length;

                $("#list-group-flush").empty();
                for (var i = 0; i < len; i++) {
                    var jour = response['periods_medecins'][i]['jour'];
                    var heureDebut = response['periods_medecins'][i]['heureDebut'];
                    var heureFin = response['periods_medecins'][i]['heureFin'];

                    $("#list-group-flush").append("<li class='list-group-item'>" + "<b>" + jour + "</b>" + "&#032;" + "<span style='float:right;'>" + heureDebut + "</span>" + "</li>");
                }
            }
        });

    });
});

// Gender select readonly
$('#gender').css('pointer-events','none');

$(document).ready(function () {
    // Handler modal for justification leaves
    $('.edit_profil_document').on('click', function () {
        var link = $(this)
        doc_type = link.data('doc');
        if (doc_type == 'id_photo') $("#updateProfileDocumentModalLabel").html("Photo d'identité");
        if (doc_type == 'cni') $("#updateProfileDocumentModalLabel").html("Carte Nationalité d’Identité (CNI)");
        if (doc_type == 'dgd_assignment_decision') $("#updateProfileDocumentModalLabel").html("Décision d’affectation de la DGD");
        if (doc_type == 'service_start_report') $("#updateProfileDocumentModalLabel").html("Compte rendu de la prise de service");
        if (doc_type == 'mudci_access_card') $("#updateProfileDocumentModalLabel").html("Carte d’accès MUDCI");
        $("#doc_type").val(doc_type);
    });
});

$(document).ready(function () {
    // Post contact message from website homepage
    $("#updateProfileDocumentForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var formData = new FormData(this);
        var url = form.attr('action');
        $('.upload_profile_doc_error_message_alert').css('display','none');
        $('.upload_profile_doc_success_message_alert').css('display','none');

        // disable button
        form.find("button[type='submit']").prop('disabled',true);
        // add spinner to button
        form.find("button[type='submit']").html('<i class="fa fa-circle-o-notch fa-spin"></i> loading...');

        $.ajax({
            type: "POST",
            url: url,
            // data: form.serialize(), // serializes the form's elements.
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',

            success: function (response) {
                form.find("button[type='submit']").prop('disabled',false);
                form.find("button[type='submit']").html('Soumettre');
                form.trigger("reset");

                if (response.error_message) {
                    // alertify.error(response.error_message);
                    $.each(response.error_message, function(index, value){
                        $("#error_message").append(value);
                    });

                    $('.upload_profile_doc_error_message_alert').css('display','block');
                }

                if (response.success_message) {
                    // alertify.success(response.success_message);
                    $.each(response.success_message, function(index, value){
                        $("#success_message").append(value);
                    });

                    $('.edit_profil_document[data-doc]').each(function(){
                        if (form.find('input[name="doc_type"]').val() == $(this).data('doc')) {
                            $(this).html('<i class="fa fa-check-circle" style="color: green;"></i>')
                        }
                    });

                    $('.upload_profile_doc_success_message_alert').css('display','block');
                }
            }
        });
    });
});

$(document).ready(function () {
    $('#my_requests').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
        }
    });
});

function ShowHideMe(RadioChecked) {
    var DivToShowHide = document.getElementById("DivToShowHide");

    if(document.getElementById("inlineRadio1").checked) {
        DivToShowHide.style.display = "block";
    }
    else if(document.getElementById("inlineRadio2").checked) {
        DivToShowHide.style.display = "block";
    }
}
