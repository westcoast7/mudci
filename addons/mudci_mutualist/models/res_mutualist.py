# -*- coding: utf-8 -*-

import base64
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.modules.module import get_module_resource
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class ResMutualist(models.Model):
    """ Temporary database of mutualists. """

    _name = 'res.mutualist'
    _description = "Base temporaire des mutualistes"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = "id desc"
    _rec_name = "register_number"

    @api.model
    def _default_image(self):
        image_path = get_module_resource('hr', 'static/src/img', 'default_image.png')
        return base64.b64encode(open(image_path, 'rb').read())

    last_name = fields.Char(string="Last Name", store=True, readonly=False, tracking=True)
    first_name = fields.Char(string="First Name", store=True, readonly=False, tracking=True)

    direction = fields.Char('Direction')
    service = fields.Char('Service')
    job_id = fields.Many2one('hr.job', 'Job Position', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    job_title = fields.Char("Job Title", compute="_compute_job_title", store=True, readonly=False)
    company_id = fields.Many2one('res.company', 'Company')
    work_phone = fields.Char('Work Phone', store=True, readonly=False)
    mobile_phone = fields.Char('Work Mobile')
    work_email = fields.Char('Work Email')
    work_location = fields.Char('Work Location')
    user_id = fields.Many2one('res.users')

    register_number = fields.Char(string="Register Number")
    code_id = fields.Char(string='Code ID', tracking=True)
    birthday = fields.Date(string='Date of Birth', tracking=True)
    country_of_birth = fields.Many2one('res.country', string="Country of Birth", tracking=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], tracking=True)
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('cohabitant', 'Legal Cohabitant'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Marital Status', tracking=True)
    phone_3 = fields.Char('Phone 3')
    phone_4 = fields.Char('Phone 4')
    function = fields.Char('Function')
    image_1920 = fields.Image(default=_default_image)

    cni = fields.Binary('Nationality Identity Card', attachment=True)
    dgd_assignment_decision = fields.Binary('DGD assignment decision', attachment=True)
    service_start_report = fields.Binary('Service start report', attachment=True)
    mudci_access_card = fields.Binary('MUDCI access card', attachment=True)
    id_photo = fields.Binary('ID photo', attachment=True)
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'res.mutualist')],
                                     string='Attachments')

    # _sql_constraints = [
    #     ('code_id_uniq', 'unique (code_id)',
    #      'The code_id of the mutualist must be unique !'),
    #     ('register_number_uniq', 'unique (register_number)',
    #      'The register_number of the mutualist must be unique !')
    # ]

    @api.depends('job_id')
    def _compute_job_title(self):
        for employee in self.filtered('job_id'):
            employee.job_title = employee.job_id.name
