# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class FormToDownload(models.Model):
    _name = 'form.to.download'
    _description = "Form to download"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'id desc'

    document_access = fields.Selection([('free', 'Free'), ('secured', 'Secured')], string='Document access')
    dfile = fields.Binary(string='Document to download')
    dfile_name = fields.Char(string='Name of the document to download')
    user_id = fields.Many2one('res.users', string='Add by', default=lambda self: self.env.uid)
