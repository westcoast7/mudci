# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class IdentificationHealthInsuranceCard(models.Model):
    """ Identification Health Insurance Card model. """

    _name = "identification.health.insurance.card"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'identification_health_insurance_card_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('identification.health.insurance.card') or '/'
        result = super(IdentificationHealthInsuranceCard, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(IdentificationHealthInsuranceCard, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_identification_health_insurance_card')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)
        _logger.info('====================================================== SEND %s', template_rec.name)
        return res

    def action_validate(self):
        res = super(IdentificationHealthInsuranceCard, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_identification_health_insurance_card')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(IdentificationHealthInsuranceCard, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_identification_health_insurance_card')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_identification_health_insurance_card_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'identification.health.insurance.card',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class IdPhotoChangeForm(models.Model):
    """ Id Photo Change Form model.

    """

    _name = "id.photo.change.form"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'photo_change_form_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('id.photo.change.form') or '/'
        result = super(IdPhotoChangeForm, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(IdPhotoChangeForm, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_id_photo_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(IdPhotoChangeForm, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_id_photo_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(IdPhotoChangeForm, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_id_photo_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_id_photo_change_form_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'id.photo.change.form',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class RegistrationNumberChangeForm(models.Model):
    """ Registration Number Change Form model.

    """

    _name = "registration.number.change.form"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'registration_number_change_form_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('registration.number.change.form') or '/'
        result = super(RegistrationNumberChangeForm, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(RegistrationNumberChangeForm, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_registration_number_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(RegistrationNumberChangeForm, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_registration_number_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(RegistrationNumberChangeForm, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_registration_number_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_registration_number_change_form_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'registration.number.change.form',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class NameChangeForm(models.Model):
    """ Name Change Form model.

    """

    _name = "name.change.form"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'name_change_form_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('name.change.form') or '/'
        result = super(NameChangeForm, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(NameChangeForm, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_name_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(NameChangeForm, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_name_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(NameChangeForm, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_name_change_form')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_name_change_form_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'name.change.form',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class DuplicateCardRequest(models.Model):
    """ Duplicate Card Request model.

    """

    _name = "duplicate.card.request"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'duplicate_card_request_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('duplicate.card.request') or '/'
        result = super(DuplicateCardRequest, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(DuplicateCardRequest, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_duplicate_card_request')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(DuplicateCardRequest, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_duplicate_card_request')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(DuplicateCardRequest, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_duplicate_card_request')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_duplicate_card_request_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'duplicate.card.request',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class RequestForACertificateOfNonRoyalty(models.Model):
    """ Request For A Certificate Of Non Royalty model.

    """

    _name = "request.for.a.certificate.of.non.royalty"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'request_for_a_certificate_of_non_royalty_id',
                                      string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('request.for.a.certificate.of.non.royalty') or '/'
        result = super(RequestForACertificateOfNonRoyalty, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(RequestForACertificateOfNonRoyalty, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_request_for_a_certificate_of_non_royalty')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(RequestForACertificateOfNonRoyalty, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_request_for_a_certificate_of_non_royalty')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(RequestForACertificateOfNonRoyalty, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_request_for_a_certificate_of_non_royalty')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_request_for_a_certificate_of_non_royalty_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'request.for.a.certificate.of.non.royalty',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class LoanApplication(models.Model):
    """ Loan Application model.

    """

    _name = "loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('loan.application') or '/'
        result = super(LoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(LoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(LoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(LoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class ExceptionalLoanApplication(models.Model):
    """ Exceptional Loan Application model.

    """

    _name = "exceptional.loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'exceptional_loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('exceptional.loan.application') or '/'
        result = super(ExceptionalLoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(ExceptionalLoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_exceptional_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(ExceptionalLoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_exceptional_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(ExceptionalLoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_exceptional_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_exceptional_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'exceptional.loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class RetirementLoanApplication(models.Model):
    """ Retirement Loan Application model.

    """

    _name = "retirement.loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'retirement_loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('retirement.loan.application') or '/'
        result = super(RetirementLoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(RetirementLoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_retirement_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(RetirementLoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_retirement_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(RetirementLoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_retirement_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_retirement_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'retirement.loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class CentralLoanApplication(models.Model):
    """ Central Loan Application model.

    """

    _name = "central.loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'central_loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('central.loan.application') or '/'
        result = super(CentralLoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(CentralLoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_central_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(CentralLoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_central_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(CentralLoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_central_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_central_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'central.loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class EvidenceAfricaLoanApplication(models.Model):
    """ Evidence Africa Loan Application model.

    """

    _name = "evidence.africa.loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'evidence_africa_loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('evidence.africa.loan.application') or '/'
        result = super(EvidenceAfricaLoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(EvidenceAfricaLoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_evidence_africa_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(EvidenceAfricaLoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_evidence_africa_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(EvidenceAfricaLoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_evidence_africa_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_evidence_africa_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'evidence.africa.loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class FrenchBookstoreLoanApplication(models.Model):
    """ French Bookstore Loan Application model.

    """

    _name = "french.bookstore.loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'french_bookstore_loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('french.bookstore.loan.application') or '/'
        result = super(FrenchBookstoreLoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(FrenchBookstoreLoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_french_bookstore_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(FrenchBookstoreLoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_french_bookstore_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(FrenchBookstoreLoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_french_bookstore_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_french_bookstore_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'french.bookstore.loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class ImportExportLoanApplication(models.Model):
    """ Import Export Loan Application model.

    """

    _name = "import.export.loan.application"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'import_export_loan_application_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('import.export.loan.application') or '/'
        result = super(ImportExportLoanApplication, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(ImportExportLoanApplication, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_import_export_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(ImportExportLoanApplication, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_import_export_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(ImportExportLoanApplication, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_import_export_loan_application')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_send_document_requested_by_email(self):
        '''
            This function opens a window to compose an email, with the requests template message loaded by default
        '''
        self.ensure_one()
        template = self.env.ref('mudci_mutualist.email_template_import_export_loan_application_request_done')
        template.write({'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})

        ctx = {
            'default_model': 'import.export.loan.application',
            'default_res_id': self.id,
            'default_template_id': template.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_light',
        }

        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }


class PowerOfAttorneyAuthorization(models.Model):
    """ Power of attorney authorization model.

    """

    _name = "power.of.attorney.authorization"
    _inherit = "mudci.request"
    _rec_name = 'name'
    _order = "name, id"

    beneficiary_ids = fields.One2many('request.beneficiary', 'power_of_attorney_authorization_id', string='assigns')

    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code('power.of.attorney.authorization') or '/'
        result = super(PowerOfAttorneyAuthorization, self).create(vals)
        return result

    # ----------------------------------------
    # Actions Methods
    # ----------------------------------------

    def action_put_on_hold(self):
        res = super(PowerOfAttorneyAuthorization, self).action_put_on_hold()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_power_of_attorney_authorization')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_validate(self):
        res = super(PowerOfAttorneyAuthorization, self).action_validate()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_power_of_attorney_authorization')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res

    def action_cancel(self):
        res = super(PowerOfAttorneyAuthorization, self).action_cancel()

        # Notify Mutualist To Request Evolution
        template_rec = self.env.ref('mudci_mutualist.email_template_power_of_attorney_authorization')
        template_rec.write({'email_to': self.partner_id.email, 'email_from': tools.formataddr((self.env.company.name, self.env.company.email))})
        template_rec.send_mail(self.id, force_send=True)

        return res


class BeneficiaryInheritance(models.Model):
    _inherit = "request.beneficiary"

    identification_health_insurance_card_id = fields.Many2one('identification.health.insurance.card', string='Request')
    photo_change_form_id = fields.Many2one('id.photo.change.form', string='Request')
    registration_number_change_form_id = fields.Many2one('registration.number.change.form', string='Request')
    name_change_form_id = fields.Many2one('name.change.form', string='Request')
    duplicate_card_request_id = fields.Many2one('duplicate.card.request', string='Request')
    request_for_a_certificate_of_non_royalty_id = fields.Many2one('request.for.a.certificate.of.non.royalty', string='Request')
    loan_application_id = fields.Many2one('loan.application', string='Request')
    exceptional_loan_application_id = fields.Many2one('exceptional.loan.application', string='Request')
    retirement_loan_application_id = fields.Many2one('retirement.loan.application', string='Request')
    central_loan_application_id = fields.Many2one('central.loan.application', string='Request')
    evidence_africa_loan_application_id = fields.Many2one('evidence.africa.loan.application', string='Request')
    french_bookstore_loan_application_id = fields.Many2one('french.bookstore.loan.application', string='Request')
    import_export_loan_application_id = fields.Many2one('import.export.loan.application', string='Request')
    power_of_attorney_authorization_id = fields.Many2one('power.of.attorney.authorization', string='Request')
