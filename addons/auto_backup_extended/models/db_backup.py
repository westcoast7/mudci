import base64
import os
import datetime
import time
import shutil
import json
import tempfile
import socket
import requests

from odoo.addons.google_drive.models.google_drive import GoogleDrive
from odoo import models, fields, api, tools, _
from odoo.exceptions import Warning, AccessDenied
import odoo

import logging

from requests.exceptions import RequestException, Timeout, ConnectionError

_logger = logging.getLogger(__name__)

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP. '
        'Please install paramiko on your system. (sudo pip3 install paramiko)')


class DbBackupExtended(models.Model):
    _inherit = 'db.backup'

    drive_folder_id = fields.Char(string='Folder ID',
                                  help="make a folder on drive in which you want to upload files; then open that folder; the last thing in present url will be folder id")
    google_drive_share_link = fields.Char(string='Drive share link')
    bkp_file = fields.Char(string='Backup file')

    @api.depends('google_drive_authorization_code')
    def _compute_drive_uri(self):
        google_drive_uri = self.env['google.service']._get_google_token_uri('drive', scope=self.env[
            'google.drive.config'].get_google_scope())
        for config in self:
            config.google_drive_uri = google_drive_uri

    def set_values(self):
        params = self.env['ir.config_parameter'].sudo()
        authorization_code_before = params.get_param('google_drive_authorization_code')
        super(DbBackupExtended, self).set_values()
        authorization_code = self.google_drive_authorization_code
        refresh_token = False
        if authorization_code and authorization_code != authorization_code_before:
            refresh_token = self.env['google.service'].generate_refresh_token('drive', authorization_code)
        params.set_param('google_drive_refresh_token', refresh_token)

    @api.model
    def schedule_backup(self):
        """ Extended scheduled database backup feature.
            Added features:
            - Sending the backup to Google Drive in a folder configured by the module manager
            - Sending of an e-mail notification after the successful operation on the e-mail
            address configured by the module manager
        """

        res = super(DbBackupExtended, self).schedule_backup()
        conf_ids = self.search([])

        # Get folder path
        for r in conf_ids:
            directory = r.folder

            try:
                # Get most recent file into backup path
                backup_files = os.listdir(directory)
                paths = [os.path.join(directory, basename) for basename in backup_files]
                file = max(paths, key=os.path.getctime)
            except Exception as error:
                _logger.debug("Exact error from the exception: %s", str(error))
                continue

            # Check if file has zip or dump extension
            if os.path.isfile(file) and (".dump" in file or '.zip' in file):
                g_drive = self.env['google.drive.config']
                access_token = GoogleDrive.get_access_token(g_drive)

                try:
                    # Google drive upload
                    headers = {"Authorization": "Bearer %s" % access_token}
                    para = {
                        "name": "%s" % (str(os.path.basename(file))),
                        "parents": ["%s" % (str(r.drive_folder_id))]
                    }
                    files = {
                        'data': ('metadata', json.dumps(para), 'application/json; charset=UTF-8'),
                        'file': open("%s" % (str(file)), "rb")
                    }
                    request = requests.post(
                        "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
                        headers=headers,
                        files=files
                    )
                except RequestException as error:
                    _logger.warning('Connection error %s' % error)
                    continue

                if request.status_code == 200:
                    # Update current object with google_drive_share_link and bkp_file data
                    r.bkp_file = str(os.path.basename(file))
                    r.google_drive_share_link = 'https://drive.google.com/drive/u/0/folders/%s' % (
                        str(r.drive_folder_id))
                    try:
                        # Send Backup file to email
                        template_rec = self.env.ref('auto_backup_extended.db_backup_email_template').sudo()
                        template_rec.write({'email_to': r.email_to_notify})
                        template_rec.send_mail(r.id, force_send=True)
                    except Exception:
                        pass

        return res
