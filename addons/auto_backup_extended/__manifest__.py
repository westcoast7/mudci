# -*- coding: utf-8 -*-
{
    'name': "Database auto-backup Extended",

    'summary': """
        Database auto backup""",

    'description': """
        Database auto backup
    """,

    'author': "Veone",
    'website': "https://www.veone.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Administration',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'auto_backup', 'google_drive'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/backup_views.xml',
        'data/mail_templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
