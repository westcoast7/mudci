# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class PowerOfAttorneyAuthorizationReport(models.AbstractModel):
    _name = 'report.mudci_mutualist.report_power_of_attorney_authorization'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['res.partner'].browse(docids)
        return {
            'doc_ids': docids,
            'doc_model': 'res.partner',
            'docs': docs,
        }
