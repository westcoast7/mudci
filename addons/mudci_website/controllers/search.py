# -*- coding: utf-8 -*-

import logging

from odoo import http
from odoo.http import request
from odoo.addons.website.controllers.main import Website as Controllers
from odoo.tools.translate import _

try:
    from html2text import html2text
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP. '
        'Please install paramiko on your system. (sudo pip3 install paramiko)')

_logger = logging.getLogger(__name__)

controllers = Controllers()


class WebsiteSearch(http.Controller):
    _results_per_page = 10
    _max_text_content_len = 500
    _text_segment_back = 50
    _text_segment_forward = 100
    _min_search_len = 3
    _search_on_pages = True
    _search_on_blogposts = True
    _search_on_comments = True
    _search_on_customers = True
    _search_on_jobs = True
    _search_on_products = True
    _case_sensitive = False
    _search_advanced = False

    def _removeSymbols(self, html_txt, symbol1, symbol2=False):

        if not symbol1 and not symbol2:
            return html_txt

        # Function to eliminate text between: symbol1 and symbol2
        index = html_txt.find(symbol1)
        start = 0
        txt = ''
        while index > 0:
            if symbol2:
                index2 = html_txt.find(symbol2, index)
                if index2 <= 0:
                    break
            else:
                index2 = index + len(symbol1) - 1
            txt += html_txt[start:index]
            start = index2 + 1
            index = html_txt.find(symbol1, start)

        if len(txt) == 0:
            return html_txt

        return txt

    def _normalize_bool(self, param):

        res = False
        if param:
            try:
                param = int(param)
                res = not (param == 0)
            except Exception:
                res = True

        return res

    def _normalize_int(self, param):

        res = 0
        if param:
            try:
                res = int(param)
            except Exception:
                res = 0

        return res

    # Low priority
    # TODO: Include results per page option?
    # TODO: Include order criteria option?

    @http.route(['/search_results', '/search_results/page/<int:page>'], type='http', auth="public", website=True)
    def search_results(self, page=1, sorting='date', search='', **post):
        cr, uid, context = request.cr, request.uid, request.context

        search = ''.join(char for char in search if char.isalnum())
        if len(search) < self._min_search_len:
            return request.render("mudci_website.error_search_len", None)

        # Define search scope
        case_sensitive = self._case_sensitive

        if not case_sensitive:
            search_lower = search.lower()

        url = "/search_results"
        sql_query = ""

        # Check for other order criteria, if new order criteria added, add here
        if sorting == 'date':
            sql_order_by = 'result_date desc'

        # Query for PCA
        if sql_query:
            sql_query += ' UNION ALL '
        sql_query += """
                          SELECT DISTINCT 'PCA Word' as result_type, pca.id as result_id, 'Mot du PCA' as result_name, 'mudci_backend' as template_module, pca.description as template_source, '' as website_meta_description, '' as website_meta_title,  '' as website_meta_keywords, '/mot-du-pca/' as result_path, '' as result_image, '' as result_lang, '' as result_lang_text, pca.write_date as result_date
                          FROM mudci_pca_word pca """
        if case_sensitive:
            sql_query += """WHERE ( pca.name ilike '%%%s%%' or pca.pca_name ilike '%%%s%%' or pca.function ilike '%%%s%%' or pca.description  ilike '%%%s%%')""" % (search, search, search, search)
        else:
            sql_query += """WHERE ( lower(pca.name) ilike '%%%s%%' or lower(pca.pca_name) ilike '%%%s%%' or lower(pca.function) ilike '%%%s%%' or lower(pca.description) ilike '%%%s%%')""" % (search_lower, search_lower, search_lower, search_lower)

        # Query for MUDCI News
        if sql_query:
            sql_query += ' UNION ALL '
        sql_query += """
                          SELECT DISTINCT 'MUDCI News' as result_type, nw.id as result_id, 'Actualites' as result_name, 'mudci_backend' as template_module, nw.description as template_source, '' as website_meta_description, '' as website_meta_title,  '' as website_meta_keywords, '/actualite/'||nw.slug||'/' as result_path, '' as result_image, '' as result_lang, '' as result_lang_text, nw.write_date as result_date
                          FROM mudci_news nw """
        if case_sensitive:
            sql_query += """WHERE ( nw.name ilike '%%%s%%' or nw.description  ilike '%%%s%%')""" % (search, search)
        else:
            sql_query += """WHERE ( lower(nw.name) ilike '%%%s%%' or lower(nw.description) ilike '%%%s%%')""" % (search_lower, search_lower)

        # Query for MUDCI Gallery
        if sql_query:
            sql_query += ' UNION ALL '
        sql_query += """
                          SELECT DISTINCT 'MUDCI Gallery' as result_type, mg.id as result_id, 'Galerie photos' as result_name, 'mudci_backend' as template_module, mg.description as template_source, '' as website_meta_description, '' as website_meta_title,  '' as website_meta_keywords, '/album/'||mg.id||'/details/' as result_path, '' as result_image, '' as result_lang, '' as result_lang_text, mg.write_date as result_date
                          FROM mudci_gallery mg """
        if case_sensitive:
            sql_query += """WHERE ( mg.name ilike '%%%s%%' or mg.description  ilike '%%%s%%')""" % (search, search)
        else:
            sql_query += """WHERE ( lower(mg.name) ilike '%%%s%%' or lower(mg.description) ilike '%%%s%%')""" % (search_lower, search_lower)

        # Query for MUDCI Web TV
        if sql_query:
            sql_query += ' UNION ALL '
        sql_query += """
                          SELECT DISTINCT 'MUDCI Web TV' as result_type, mv.id as result_id, 'Web TV' as result_name, 'mudci_backend' as template_module, mv.description as template_source, '' as website_meta_description, '' as website_meta_title,  '' as website_meta_keywords, '/videotheque/'||mv.slug||'/' as result_path, '' as result_image, '' as result_lang, '' as result_lang_text, mv.write_date as result_date
                          FROM mudci_videotec mv """
        if case_sensitive:
            sql_query += """WHERE ( mv.name ilike '%%%s%%' or mv.description  ilike '%%%s%%')""" % (search, search)
        else:
            sql_query += """WHERE ( lower(mv.name) ilike '%%%s%%' or lower(mv.description) ilike '%%%s%%')""" % (search_lower, search_lower)

        # Build query count
        if sql_query:
            sql_query_count = """SELECT count(distinct result_type||'-'||result_id  ) FROM ( %s ) as subquery""" % (sql_query)

        # Build query for results ordered
        if sql_query:
            limit = self._results_per_page
            offset = (page - 1) * self._results_per_page
            sql_query_ordered = """SELECT distinct result_type, result_id, result_name,  template_module, template_source, website_meta_description, website_meta_title, website_meta_keywords, result_path, result_image, result_lang, result_lang_text, result_date
                                 FROM ( %s ) as subquery
                                 ORDER BY %s
                                 LIMIT %s
                                 OFFSET %s
                                 """ % (sql_query, sql_order_by, limit, offset)

        # Get results count for pager
        if sql_query_count:
            cr.execute(sql_query_count)
            results_count = cr.fetchone()[0] or 0

        url_args = {}
        if search:
            url_args['search'] = search

        if sorting:
            url_args['sorting'] = sorting
        pager = request.website.pager(url=url, total=results_count, page=page, step=self._results_per_page, scope=self._results_per_page, url_args=url_args)

        # Get results and prepare info to render results page
        user = request.env['res.users'].sudo().browse(request.env.user.id)
        values = {'user': user,
                  'is_public_user': user.id == request.website.user_id.id,
                  'header': post.get('header', dict()),
                  'searches': post.get('searches', dict()),
                  'results_per_page': self._results_per_page,
                  'last_result_showing': min(results_count, page * self._results_per_page),
                  'results_count': results_count,
                  'results': [],
                  'pager': pager,
                  'case_sensitive': self._case_sensitive,
                  'sorting': sorting,
                  'search': search,
                  }

        if sql_query_ordered:
            cr.execute(sql_query_ordered)
            for result in cr.fetchall():
                result_id = result[0] + '-' + str(result[1])
                result_data = {
                    'type': result[0],
                    'type_txt': _(result[0]),
                    'id': result[1],
                    'name': result[2],
                    'template_name': '',
                    'template_source': result[4],
                    'website_meta_description': result[5],
                    'website_meta_title': result[6],
                    'website_meta_keywords': result[7],
                    'url': result[8],
                    'image': result[9],  # seleccionar una imagen por tipo
                    'lang': result[10],
                    'lang_text': result[11],
                    'date': result[12].strftime("%d/%m/%Y"),
                    'ocurrences': 0,
                    'object': None
                }
                
                # Prepare result content near searched keyword
                if result_data['type'] == 'PCA Word':
                    # Render product info html
                    try:
                        pca_word = request.env['mudci.pca.word'].sudo().browse(int(result_data['id']))
                        result_data['object'] = pca_word
                        html = request.env['ir.ui.view'].sudo()._render_template("mudci_website.search_job_detail", {'pca_word': pca_word})
                    except Exception:
                        html = '<main>' + _('Unable to get product text') + '</main>'
                        html = html.encode('utf-8')

                    start = 0
                    end = len(html)

                elif result_data['type'] == 'MUDCI News':
                    # Render product info html
                    try:
                        new = request.env['mudci.news'].sudo().browse(int(result_data['id']))
                        result_data['object'] = new
                        html = request.env['ir.ui.view'].sudo()._render_template("mudci_website.search_news", {'new': new})
                    except Exception:
                        html = '<main>' + _('Unable to get actuality text') + '</main>'
                        html = html.encode('utf-8')

                    start = 0
                    end = len(html)
                    
                elif result_data['type'] == 'MUDCI Gallery':
                    # Render product info html
                    try:
                        gallery = request.env['mudci.gallery'].sudo().browse(int(result_data['id']))
                        result_data['object'] = gallery
                        html = request.env['ir.ui.view'].sudo()._render_template("mudci_website.search_galleries", {'gallery': gallery})
                    except Exception:
                        html = '<main>' + _('Unable to get gallery text') + '</main>'
                        html = html.encode('utf-8')

                    start = 0
                    end = len(html)

                elif result_data['type'] == 'MUDCI Web TV':
                    # Render product info html
                    try:
                        video = request.env['mudci.videotec'].sudo().browse(int(result_data['id']))
                        result_data['object'] = video
                        html = request.env['ir.ui.view'].sudo()._render_template("mudci_website.search_web_tv", {'video': video})
                    except Exception:
                        html = '<main>' + _('Unable to get gallery text') + '</main>'
                        html = html.encode('utf-8')

                    start = 0
                    end = len(html)

                # Keep key part of html page
                html = html[start:end]

                # Convert to text, eliminate all tags and #, \n, [, ] symbols, and text between []
                if html2text:
                    html = html2text(html.decode('utf-8')).encode('utf-8')
                    html = self._removeSymbols(html.decode('utf-8'), '[', ']').encode('utf-8')
                    html = self._removeSymbols(html.decode('utf-8'), '\n').encode('utf-8')
                    html = self._removeSymbols(html.decode('utf-8'), '#').encode('utf-8')

                html = html.decode("utf-8")
                # If not case sensitive search, apply lower function to search term and html
                if case_sensitive:
                    search_term = search
                    search_html = html
                else:
                    search_term = search_lower
                    search_html = html.lower()

                # Trim content to a maximum total characters to show in description with nearest text
                if len(search_html) > self._max_text_content_len:
                    index = search_html.find(str(search_term), 0)
                    start = max(0, index - self._text_segment_back)
                    end = min(len(search_html), index + self._text_segment_forward)
                    html_trim = html[start:end]
                    search_html_trim = search_html[start:end]
                    if start > 0:
                        html_trim = "..." + html_trim
                        search_html_trim = "..." + search_html_trim
                    if end < len(search_html):
                        html_trim = html_trim + "..."
                        search_html_trim = search_html_trim + "..."
                    search_html = search_html_trim
                    html = html_trim

                # Find keyword in description text to force style to background yellow and bold text
                index = search_html.find(str(search_term), 0)
                index_start = 0
                str_styled_search = "<span style='font-weight: bold; font-size: 100%%; background-color: yellow;'>%s</span>" % str(search)
                html_styled = ''
                ocurrences = 0
                while index >= 0:
                    ocurrences += 1
                    html_styled += html[index_start:index]
                    html_styled += str_styled_search
                    index_start = index + len(str(search_term))
                    index = search_html.find(str(search_term), index_start)
                html_styled += html[index_start:]

                result_data['content'] = "<p>" + html_styled + "</p>"
                result_data['ocurrences'] = ocurrences
                values['results'].append(result_data)

        # Render results
        return request.render("mudci_website.search_results", values)
