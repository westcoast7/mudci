# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import collections
import datetime
import hashlib
import pytz
import threading
import re

import requests
from lxml import etree
from random import randint
from werkzeug import urls

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.modules import get_module_resource
from odoo.osv.expression import get_unaccent_wrapper
from odoo.exceptions import UserError, ValidationError


class Partner(models.Model):
    _description = 'Mutualistes'
    _inherit = "res.partner"
    _order = "display_name"

    last_name = fields.Char(string="Last Name", store=True, readonly=False, tracking=True)
    first_name = fields.Char(string="First Name", store=True, readonly=False, tracking=True)

    direction = fields.Char('Direction')
    service = fields.Char('Service')
    job_id = fields.Many2one('hr.job', 'Job Position', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    job_title = fields.Char("Job Title", store=True, readonly=False)
    work_location = fields.Char('Work Location')

    register_number = fields.Char(string="Register Number")
    code_id = fields.Char(string='Code ID', tracking=True)
    birthday = fields.Date(string='Date of Birth', tracking=True)
    country_of_birth = fields.Many2one('res.country', string="Country of Birth", tracking=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], tracking=True)
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('cohabitant', 'Legal Cohabitant'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Marital Status', tracking=True)
    phone_3 = fields.Char('Phone 3')
    phone_4 = fields.Char('Phone 4')

    cni = fields.Binary('Nationality Identity Card')
    cni_name = fields.Char('CNI Name')
    dgd_assignment_decision = fields.Binary('DGD assignment decision')
    dgd_assignment_decision_name = fields.Char('DGD assignment decision Name')
    service_start_report = fields.Binary('Service start report')
    service_start_report_name = fields.Char('Service start report Name')
    mudci_access_card = fields.Binary('MUDCI access card')
    mudci_access_card_name = fields.Char('MUDCI access card Name')
    id_photo = fields.Binary('ID photo')
    id_photo_name = fields.Char('ID photo Name')
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'res.partner')], string='Attachments')
    is_mutualist = fields.Boolean(string='Est un Mutualiste', default=False)

    identification_health_insurance_card_ids = fields.One2many('identification.health.insurance.card', 'partner_id', string='Requests')
    photo_change_form_ids = fields.One2many('id.photo.change.form', 'partner_id', string='Requests')
    registration_number_change_form_ids = fields.One2many('registration.number.change.form', 'partner_id', string='Requests')
    name_change_form_ids = fields.One2many('name.change.form', 'partner_id', string='Requests')
    duplicate_card_request_ids = fields.One2many('duplicate.card.request', 'partner_id', string='Requests')
    request_for_a_certificate_of_non_royalty_ids = fields.One2many('request.for.a.certificate.of.non.royalty', 'partner_id', string='Requests')
    loan_application_ids = fields.One2many('loan.application', 'partner_id', string='Requests')
    exceptional_loan_application_ids = fields.One2many('exceptional.loan.application', 'partner_id', string='Requests')
    retirement_loan_application_ids = fields.One2many('retirement.loan.application', 'partner_id', string='Requests')
    central_loan_application_ids = fields.One2many('central.loan.application', 'partner_id', string='Requests')
    evidence_africa_loan_application_ids = fields.One2many('evidence.africa.loan.application', 'partner_id', string='Requests')
    french_bookstore_loan_application_ids = fields.One2many('french.bookstore.loan.application', 'partner_id', string='Requests')
    import_export_loan_application_ids = fields.One2many('import.export.loan.application', 'partner_id', string='Requests')
    power_of_attorney_authorization_ids = fields.One2many('power.of.attorney.authorization', 'partner_id', string='Requests')
