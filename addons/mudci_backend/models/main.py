# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class MudciHomepageMobileAppSection(models.Model):
    """ Mudci Homepage Mobile App Section model. """

    _name = "mudci.homepage.mobileapp.section"
    _description = "MUDCI Homepage Mobile App Section"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    playstore_link = fields.Char(string='Playstore link')
    playstore_btn_name = fields.Char(string='Playstore button name')
    appstore_link = fields.Char(string='Appstore link')
    appstore_btn_name = fields.Char(string='Appstore button name')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_642 = fields.Image("Image 642", related="image_1920", max_width=642, max_height=642, store=True)


class MudciHomepageMobileAppSection(models.Model):
    """ Mudci Word from the PCA. """

    _name = "mudci.pca.word"
    _description = "MUDCI Word from the PCA"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    short_description = fields.Html(string='Courte description')
    pca_name = fields.Char(string='PCA Name')
    function = fields.Char(string='Function')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_768 = fields.Image("Image 768", related="image_1920", max_width=768, max_height=768, store=True)


class MudciHistorical(models.Model):
    """ Mudci Historical. """

    _name = "mudci.historical"
    _description = "MUDCI Historical"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class MudciOrganisation(models.Model):
    """ Mudci Organisation. """

    _name = "mudci.organisation"
    _description = "MUDCI Organisation"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class MudciBeneficiary(models.Model):
    """ Mudci Beneficiary. """

    _name = "mudci.beneficiary"
    _description = "MUDCI Beneficiary"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class MudciFaq(models.Model):
    """ Mudci FAQ. """

    _name = "mudci.faq"
    _description = "MUDCI FAQ"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin']
    _order = 'id desc'

    ask = fields.Text(string='Ask')
    answer = fields.Text(string='Answer')
    active = fields.Boolean(string='Active', default=True)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MudciQuestionsFromPortalUsers(models.Model):
    """ Mudci Questions from portal users. """

    _name = "mudci.portal.user.question"
    _description = "MUDCI Questions from portal users"
    _order = 'id desc'

    name = fields.Char(string='Name', required=True)
    email = fields.Char(string='Email', required=True)
    phone = fields.Char(string='Phone number')
    ask = fields.Text(string='Ask')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class UsefulLinks(models.Model):
    """ Mudci useful links. """

    _name = "mudci.useful.links"
    _description = "MUDCI Useful links"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    link = fields.Char(string='Link')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)
    image_80 = fields.Image("Image 80", related="image_1920", max_width=80, max_height=80, store=True)
