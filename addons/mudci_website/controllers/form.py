# -*- coding: utf-8 -*-

import base64
import json
import logging
import requests
from datetime import datetime, timedelta
from pytz import timezone, UTC
from datetime import time

from odoo import _
from odoo import http
from odoo import tools
from odoo.http import Controller, request, route

_logger = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class OnlineRequests(Controller):
    INSURANCE_REQUEST_FIELDS = [
        "id_photo",
        "subscriber_name",
        "last_name",
        "first_name",
        "registration_number",
        "id_code",
        "beneficiary_name",
        "birthday",
        "family_status",
        "coverage",
        "phone_number",
        "deposit_date",
        "birth_certificate",
        "mudci_card",
    ]

    ID_PHOTO_FORM_REQUEST_FIELDS = [
        "id_photo",
        "last_name",
        "first_name",
        "registration_number",
        "id_code",
        "phone_number",
    ]

    REGISTRATION_NUMBER_CHANGE_FORM_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "id_code",
        "new_registration_number",
        "phone_number",
        "assignment_decision_bearing_the_new_number",
        "service_report",
    ]

    NAME_CHANGE_FORM_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "id_code",
        "phone_number",
        "new_last_name",
        "new_first_name",
        "act_of_change_of_name",
    ]

    DUPLICATE_CARD_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "id_code",
        "phone_number",
        "reason_for_the_duplicate_request",
        "last_name_of_the_duplicate_requester",
        "first_name_of_the_duplicate_requester",
        "registration_number_of_the_duplicate_requester",
        "family_status_of_the_duplicate_requester",
    ]

    REQUEST_FOR_A_CERTIFICATE_OF_NON_ROYALITY_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "service",
        "function",
        "direction",
        "phone_number",
    ]

    LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "deduction_quarter",
        "reason",
        "mudci_card",
        "cni",
    ]

    EXCEPTIONAL_LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "deduction_quarter",
        "reason",
        "mudci_card",
        "cni",
    ]

    RETIREMENT_LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "retirement_year",
        "reason",
        "mudci_card",
        "cni",
    ]

    CENTRAL_LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "to_count_in",
        "reason",
        "mudci_card",
        "cni",
        "proforma_invoice",
    ]

    EVIDENCE_AFRICA_LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "to_count_in",
        "reason",
        "mudci_card",
        "cni",
        "proforma_invoice",
    ]

    FRENCH_BOOKSTORE_LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "mudci_card",
        "cni",
    ]

    IMPORT_EXPORT_LOAN_APPLICATION_REQUEST_FIELDS = [
        "last_name",
        "first_name",
        "registration_number",
        "phone_number",
        "function",
        "service",
        "amount_of_the_loan",
        "to_count_in",
        "for_the_acquisition_of",
        "mudci_card",
        "cni",
        # "bounty_slip",
        "proforma_invoice",
    ]

    POWER_OF_ATTORNEY_AUTHORIZATION_FIELDS = [
        "power_of_attorney_authorization",
    ]

    NO_REQUIRED_FIELDS = [
        "beneficiary_name",
        "marriage_certificate_for_the_married_woman",
        "cni",
        "birth_certificate",
        "cni_or_birth_certificate",
        "damaged_card_or_certificate_of_loss",
        "ufile",
        "effective_date",
    ]

    def details_form_validate(self, data, request_fields):
        error = dict()
        error_message = []

        # Validation
        for field_name in request_fields:
            if not data.get(field_name):
                error[field_name] = 'missing'

        # error message for empty required fields
        if [err for err in error.values() if err == 'missing']:
            error_message.append(_('Some required fields are empty.'))

        unknown = [k for k in data if k not in request_fields and k not in self.NO_REQUIRED_FIELDS]
        if unknown:
            error['common'] = 'Unknown field'
            error_message.append("Unknown field '%s'" % ','.join(unknown))

        return error, error_message

    @http.route('/formulaires-a-telecharger', auth='user', website=True)
    def secured_forms_to_download(self, **kw):
        secured_forms_to_download = request.env['form.to.download'].sudo().search([('document_access', '=', 'secured')],
                                                                                  order='id desc')

        values = {
            'secured_forms_to_download': secured_forms_to_download,
        }

        return http.request.render('mudci_website.secured_forms_to_download', values)

    @http.route('/formulaires-a-renseigner-en-ligne', auth='user', website=True)
    def forms_to_be_completed_online(self, **kw):
        return http.request.render('mudci_website.forms_to_be_completed_online')

    @http.route('/mes-demandes-en-ligne', auth='user', website=True)
    def my_online_requests(self, **kw):
        return http.request.render('mudci_website.my_online_requests')

    @http.route('/nos-ressources', auth='public', website=True)
    def free_forms_to_download(self, **kw):
        free_forms_to_download = request.env['form.to.download'].sudo().search([('document_access', '=', 'free')],
                                                                               order='id desc')
        free_forms_to_download_banner = request.env['our.resources.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                     order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'free_forms_to_download': free_forms_to_download,
            'free_forms_to_download_banner': free_forms_to_download_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.free_forms_to_download', values)

    @http.route('/fiche-identification-carte-assurance-maladie', auth='user', website=True)
    def identification_card_health_insurance_card(self, **post):
        partner = request.env.user.partner_id
        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            identification_health_insurance_card_obj = request.env['identification.health.insurance.card'].sudo()
            error, error_message = self.details_form_validate(post, self.INSURANCE_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.INSURANCE_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    identification_health_insurance_card = identification_health_insurance_card_obj.create({
                        "subscriber_name": values['subscriber_name'],
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "id_code": values['id_code'],
                        "coverage": values['coverage'],
                        "phone_number": values['phone_number'],
                        "deposit_date": values['deposit_date'],
                        "birthday": values['birthday'],
                        "partner_id": partner.id,
                        "effective_date": values.get('effective_date'),
                        "family_status": values['family_status'],
                        'beneficiary_ids': [
                            (0, 0, {'name': values['beneficiary_name']})
                        ],
                        "request_type": "identification_health_insurance_card"
                    })

                    if post.get('id_photo', False):
                        file = request.httprequest.files.getlist('id_photo')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                identification_health_insurance_card.sudo().write({
                                    'id_photo': base64.encodebytes(file.read()),
                                    'id_photo_name': file.filename
                                })

                    if post.get('birth_certificate', False):
                        file = request.httprequest.files.getlist('birth_certificate')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                identification_health_insurance_card.sudo().write({
                                    'birth_certificate': base64.encodebytes(file.read()),
                                    'birth_certificate_name': file.filename
                                })
                                
                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                identification_health_insurance_card.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    template_rec = request.env.ref(
                        'mudci_website.email_template_identification_health_insurance_card').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(identification_health_insurance_card.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.identification_card_health_insurance_card', values)

    @http.route('/fiche-changement-photo', auth='user', website=True)
    def id_photo_change_form(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            id_photo_change_form_obj = request.env['id.photo.change.form'].sudo()
            error, error_message = self.details_form_validate(post, self.ID_PHOTO_FORM_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.ID_PHOTO_FORM_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    id_photo_change_form = id_photo_change_form_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "id_code": values['id_code'],
                        "partner_id": partner.id,
                        "phone_number": values['phone_number'],
                        "request_type": "id_photo_change_form"
                    })

                    if post.get('id_photo', False):
                        file = request.httprequest.files.getlist('id_photo')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                id_photo_change_form.sudo().write({
                                    'id_photo': base64.encodebytes(file.read()),
                                    'id_photo_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_id_photo_change_form').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(id_photo_change_form.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.id_photo_change_form', values)

    @http.route('/fiche-changement-matricule', auth='user', website=True)
    def registration_number_change_form(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            registration_number_change_form_obj = request.env['registration.number.change.form'].sudo()
            error, error_message = self.details_form_validate(post, self.REGISTRATION_NUMBER_CHANGE_FORM_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.REGISTRATION_NUMBER_CHANGE_FORM_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    registration_number_change_form = registration_number_change_form_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "id_code": values['id_code'],
                        "partner_id": partner.id,
                        "phone_number": values['phone_number'],
                        "request_type": "registration_number_change_form",
                        "new_registration_number": values['new_registration_number'],
                    })

                    if request.httprequest.form.getlist('beneficiary_name'):
                        beneficiaries_name = request.httprequest.form.getlist('beneficiary_name')
                        for name in beneficiaries_name:
                            if name:
                                registration_number_change_form.sudo().write({
                                    'beneficiary_ids': [
                                        (0, 0, {'name': name})
                                    ],
                                })

                    if post.get('assignment_decision_bearing_the_new_number', False):
                        file = request.httprequest.files.getlist('assignment_decision_bearing_the_new_number')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                registration_number_change_form.sudo().write({
                                    'assignment_decision_bearing_the_new_number': base64.encodebytes(file.read()),
                                    'assignment_decision_bearing_the_new_number_name': file.filename
                                })

                    if post.get('service_report', False):
                        file = request.httprequest.files.getlist('service_report')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                registration_number_change_form.sudo().write({
                                    'service_report': base64.encodebytes(file.read()),
                                    'service_report_name': file.filename
                                })

                    template_rec = request.env.ref(
                        'mudci_website.email_template_registration_number_change_form').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(registration_number_change_form.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.registration_number_change_form', values)

    @http.route('/fiche-changement-nom', auth='user', website=True)
    def name_change_form(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            name_change_form_obj = request.env['name.change.form'].sudo()
            error, error_message = self.details_form_validate(post, self.NAME_CHANGE_FORM_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.NAME_CHANGE_FORM_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    name_change_form = name_change_form_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "id_code": values['id_code'],
                        "partner_id": partner.id,
                        "phone_number": values['phone_number'],
                        "new_last_name": values['new_last_name'],
                        "new_first_name": values['new_first_name'],
                        "request_type": "name_change_form"
                    })

                    if post.get('cni_or_birth_certificate'):
                        if post.get('cni_or_birth_certificate') == 'cni':
                            if post.get('ufile', False):
                                file = request.httprequest.files.getlist('ufile')[0]
                                if file.filename != '':
                                    if allowed_file(file.filename):
                                        name_change_form.sudo().write({
                                            'cni': base64.encodebytes(file.read()),
                                            'cni_name': file.filename
                                        })
                        else:
                            if post.get('ufile', False):
                                file = request.httprequest.files.getlist('ufile')[0]
                                if file.filename != '':
                                    if allowed_file(file.filename):
                                        name_change_form.sudo().write({
                                            'birth_certificate': base64.encodebytes(file.read()),
                                            'birth_certificate_name': file.filename
                                        })

                    if post.get('act_of_change_of_name', False):
                        file = request.httprequest.files.getlist('act_of_change_of_name')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                name_change_form.sudo().write({
                                    'act_of_change_of_name': base64.encodebytes(file.read()),
                                    'act_of_change_of_name_name': file.filename
                                })

                    if post.get('marriage_certificate_for_the_married_woman', False):
                        file = request.httprequest.files.getlist('marriage_certificate_for_the_married_woman')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                name_change_form.sudo().write({
                                    'marriage_certificate_for_the_married_woman': base64.encodebytes(file.read()),
                                    'marriage_certificate_for_the_married_woman_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_name_change_form').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(name_change_form.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.name_change_form', values)

    @http.route('/demande-duplicata-carte-acces-mudci', auth='user', website=True)
    def duplicate_card_request(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            duplicate_card_request_obj = request.env['duplicate.card.request'].sudo()
            error, error_message = self.details_form_validate(post, self.DUPLICATE_CARD_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.DUPLICATE_CARD_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    duplicate_card_request = duplicate_card_request_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "id_code": values['id_code'],
                        "phone_number": values['phone_number'],
                        "reason_for_the_duplicate_request": values['reason_for_the_duplicate_request'],
                        "last_name_of_the_duplicate_requester": values['last_name_of_the_duplicate_requester'],
                        "first_name_of_the_duplicate_requester": values['first_name_of_the_duplicate_requester'],
                        "registration_number_of_the_duplicate_requester": values[
                            'registration_number_of_the_duplicate_requester'],
                        "family_status_of_the_duplicate_requester": values['family_status_of_the_duplicate_requester'],
                        "request_type": "duplicate_card_request",
                        "partner_id": partner.id,
                    })

                    if post.get('damaged_card_or_certificate_of_loss'):
                        if post.get('damaged_card_or_certificate_of_loss') == 'certificate_of_loss':
                            if post.get('ufile', False):
                                file = request.httprequest.files.getlist('ufile')[0]
                                if file.filename != '':
                                    if allowed_file(file.filename):
                                        duplicate_card_request.sudo().write({
                                            'certificate_of_loss': base64.encodebytes(file.read()),
                                            'certificate_of_loss_name': file.filename
                                        })
                        else:
                            if post.get('ufile', False):
                                file = request.httprequest.files.getlist('ufile')[0]
                                if file.filename != '':
                                    if allowed_file(file.filename):
                                        duplicate_card_request.sudo().write({
                                            'damaged_card': base64.encodebytes(file.read()),
                                            'damaged_card_name': file.filename
                                        })

                    template_rec = request.env.ref('mudci_website.email_template_duplicate_card_request').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(duplicate_card_request.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.duplicate_card_request', values)

    @http.route('/demande-attestation-nom-redevance', auth='user', website=True)
    def request_for_a_certificate_of_non_royalty(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            request_for_a_certificate_of_non_royalty_obj = request.env[
                'request.for.a.certificate.of.non.royalty'].sudo()
            error, error_message = self.details_form_validate(post,
                                                              self.REQUEST_FOR_A_CERTIFICATE_OF_NON_ROYALITY_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.REQUEST_FOR_A_CERTIFICATE_OF_NON_ROYALITY_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    request_for_a_certificate_of_non_royalty = request_for_a_certificate_of_non_royalty_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "service": values['service'],
                        "direction": values.get('direction'),
                        "function": values['function'],
                        "partner_id": partner.id,
                        "phone_number": values['phone_number'],
                        "request_type": "request_for_a_certificate_of_non_royalty"
                    })

                    template_rec = request.env.ref(
                        'mudci_website.email_template_request_for_a_certificate_of_non_royalty').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(request_for_a_certificate_of_non_royalty.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.request_for_a_certificate_of_non_royalty', values)

    @http.route('/demande-pret', auth='user', website=True)
    def loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            loan_application_obj = request.env['loan.application'].sudo()
            error, error_message = self.details_form_validate(post, self.LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    loan_application = loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "loan_application",
                        "function": values['function'],
                        "service": values['service'],
                        "partner_id": partner.id,
                        "amount_of_the_loan": values['amount_of_the_loan'],
                        "deduction_quarter": values['deduction_quarter'],
                        "reason": values['reason']
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.loan_application', values)

    @http.route('/demande-pret-exceptionnel', auth='user', website=True)
    def exceptional_loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            exceptional_loan_application_obj = request.env['exceptional.loan.application'].sudo()
            error, error_message = self.details_form_validate(post, self.EXCEPTIONAL_LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.EXCEPTIONAL_LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    exceptional_loan_application = exceptional_loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "exceptional_loan_application",
                        "function": values['function'],
                        "service": values['service'],
                        "partner_id": partner.id,
                        "amount_of_the_loan": values['amount_of_the_loan'],
                        "deduction_quarter": values.get('deduction_quarter'),
                        "reason": values['reason']
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                exceptional_loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                exceptional_loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_exceptional_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(exceptional_loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.exceptional_loan_application', values)

    @http.route('/demande-pret-retraite', auth='user', website=True)
    def retirement_loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            retirement_loan_application_obj = request.env['retirement.loan.application'].sudo()
            error, error_message = self.details_form_validate(post, self.RETIREMENT_LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.RETIREMENT_LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    retirement_loan_application = retirement_loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "retirement_loan_application",
                        "function": values['function'],
                        "service": values['service'],
                        "partner_id": partner.id,
                        "amount_of_the_loan": values['amount_of_the_loan'],
                        "retirement_year": values['retirement_year'],
                        "reason": values['reason']
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                retirement_loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                retirement_loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_retirement_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(retirement_loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.retirement_loan_application', values)

    @http.route('/demande-pret-central-ph6', auth='user', website=True)
    def central_loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            central_ph6_loan_application_obj = request.env['central.loan.application'].sudo()
            error, error_message = self.details_form_validate(post, self.CENTRAL_LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.CENTRAL_LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    central_ph6_loan_application = central_ph6_loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "central_ph6_loan_application",
                        "function": values['function'],
                        "service": values['service'],
                        "partner_id": partner.id,
                        "amount_of_the_loan": values['amount_of_the_loan'],
                        "to_count_in": values['to_count_in'],
                        "reason": values['reason']
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                central_ph6_loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                central_ph6_loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    if post.get('proforma_invoice', False):
                        file = request.httprequest.files.getlist('proforma_invoice')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                central_ph6_loan_application.sudo().write({
                                    'proforma_invoice': base64.encodebytes(file.read()),
                                    'proforma_invoice_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_central_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(central_ph6_loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.central_loan_application', values)

    @http.route('/demande-pret-evidence-africa', auth='user', website=True)
    def evidence_africa_loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            evidence_africa_loan_application_obj = request.env['evidence.africa.loan.application'].sudo()
            error, error_message = self.details_form_validate(post,
                                                              self.EVIDENCE_AFRICA_LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.EVIDENCE_AFRICA_LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    evidence_africa_loan_application = evidence_africa_loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "evidence_africa_loan_application",
                        "function": values['function'],
                        "service": values['service'],
                        "partner_id": partner.id,
                        "amount_of_the_loan": values['amount_of_the_loan'],
                        "to_count_in": values['to_count_in'],
                        "reason": values['reason']
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                evidence_africa_loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                evidence_africa_loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    if post.get('proforma_invoice', False):
                        file = request.httprequest.files.getlist('proforma_invoice')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                evidence_africa_loan_application.sudo().write({
                                    'proforma_invoice': base64.encodebytes(file.read()),
                                    'proforma_invoice_name': file.filename
                                })

                    template_rec = request.env.ref(
                        'mudci_website.email_template_evidence_africa_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(evidence_africa_loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.evidence_africa_loan_application', values)

    @http.route('/demande-pret-librairie-de-france', auth='user', website=True)
    def french_bookstore_loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            french_bookstore_loan_application_obj = request.env['french.bookstore.loan.application'].sudo()
            error, error_message = self.details_form_validate(post,
                                                              self.FRENCH_BOOKSTORE_LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.FRENCH_BOOKSTORE_LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    french_bookstore_loan_application = french_bookstore_loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "french_bookstore_loan_application",
                        "function": values['function'],
                        "service": values['service'],
                        "partner_id": partner.id,
                        "amount_of_the_loan": values['amount_of_the_loan'],
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                french_bookstore_loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                french_bookstore_loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    template_rec = request.env.ref(
                        'mudci_website.email_template_french_bookstore_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(french_bookstore_loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.french_bookstore_loan_application', values)

    @http.route('/demande-pret-import-export', auth='user', website=True)
    def import_export_loan_application(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            import_export_loan_application_obj = request.env['import.export.loan.application'].sudo()
            error, error_message = self.details_form_validate(post, self.IMPORT_EXPORT_LOAN_APPLICATION_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.IMPORT_EXPORT_LOAN_APPLICATION_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    import_export_loan_application = import_export_loan_application_obj.create({
                        "last_name": values['last_name'],
                        "first_name": values['first_name'],
                        "registration_number": values['registration_number'],
                        "phone_number": values['phone_number'],
                        "request_type": "3k_import_export_loan_application",
                        "function": values['function'],
                        "partner_id": partner.id,
                        "service": values['service'],
                        "amount_of_the_loan": values['amount_of_the_loan'],
                        "to_count_in": values['to_count_in'],
                        "for_the_acquisition_of": values['for_the_acquisition_of'],
                    })

                    if post.get('mudci_card', False):
                        file = request.httprequest.files.getlist('mudci_card')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                import_export_loan_application.sudo().write({
                                    'mudci_card': base64.encodebytes(file.read()),
                                    'mudci_card_name': file.filename
                                })

                    if post.get('cni', False):
                        file = request.httprequest.files.getlist('cni')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                import_export_loan_application.sudo().write({
                                    'cni': base64.encodebytes(file.read()),
                                    'cni_name': file.filename
                                })

                    if post.get('proforma_invoice', False):
                        file = request.httprequest.files.getlist('proforma_invoice')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                import_export_loan_application.sudo().write({
                                    'proforma_invoice': base64.encodebytes(file.read()),
                                    'proforma_invoice_name': file.filename
                                })

                    template_rec = request.env.ref('mudci_website.email_template_import_export_loan_application').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(import_export_loan_application.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.import_export_loan_application', values)

    @http.route('/autorisation-de-procuration', auth='user', website=True)
    def power_of_attorney_authorization(self, **post):
        partner = request.env.user.partner_id

        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            power_of_attorney_authorization_obj = request.env['power.of.attorney.authorization'].sudo()
            error, error_message = self.details_form_validate(post, self.POWER_OF_ATTORNEY_AUTHORIZATION_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.POWER_OF_ATTORNEY_AUTHORIZATION_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    power_of_attorney_authorization = power_of_attorney_authorization_obj.create({
                        "partner_id": partner.id,
                        "request_type": "power_of_attorney_authorization"
                    })

                    if post.get('power_of_attorney_authorization', False):
                        file = request.httprequest.files.getlist('power_of_attorney_authorization')[0]
                        if file.filename != '':
                            if allowed_file(file.filename):
                                power_of_attorney_authorization.sudo().write({
                                    'power_of_attorney_authorization': base64.encodebytes(file.read()),
                                    'power_of_attorney_authorization_name': file.filename
                                })

                    template_rec = request.env.ref(
                        'mudci_website.email_template_power_of_attorney_authorization').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(power_of_attorney_authorization.id, force_send=True)

                    success_message = [_('Request has sent successfully.')]
                    values.update({'success_message': success_message})
                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        values.update({
            'partner': partner,
        })

        return http.request.render('mudci_website.power_of_attorney_authorization', values)

    @http.route('/web/download/binary/<string:model>/<int:res_id>/<string:field>/<string:filename>', type='http', auth="public")
    def download_binary_document(self, model, field, res_id, filename=None, **kw):

        status, headers, content = request.env['ir.http'].sudo().binary_content(
            model=model,
            id=res_id,
            field=field,
            download=True,
            filename=filename,
            default_mimetype='application/octet-stream',
        )

        if status != 200:
            return request.env['ir.http'].sudo()._response_by_status(status, headers, content)
        else:
            content_base64 = base64.b64decode(content)
            headers.append(('Content-Length', len(content_base64)))
            response = request.make_response(content_base64, headers)

        return response

    @http.route('/web/download/report/<int:rec_id>', type='http', auth="user")
    def download_report(self, rec_id, **kw):
        partner = request.env['res.partner'].sudo().browse(rec_id)
        pdf = request.env.ref('mudci_mutualist.action_power_of_attorney_authorization').sudo()._render_qweb_pdf([partner.id])[0]
        pdfhttpheaders = [
            ('Content-Type', 'application/pdf'),
            ('Content-Length', len(pdf))
        ]

        return request.make_response(pdf, headers=pdfhttpheaders)

    @http.route('/mes-demandes/<string:model>', auth="user", website=True)
    def my_requests(self, model, **kw):
        partner = request.env.user.partner_id
        values = {
            'partner': partner,
        }

        if not model:
            return request.not_found()

        request_type = ''
        my_requests = request.env[model].sudo().search([('partner_id', '=', partner.id)])
        
        if model == 'identification.health.insurance.card':
            request_type = 'identification_health_insurance_card'
        elif model == 'id.photo.change.form':
            request_type = 'id_photo_change_form'
        elif model == 'registration.number.change.form':
            request_type = 'registration_number_change_form'
        elif model == 'name.change.form':
            request_type = 'name_change_form'
        elif model == 'duplicate.card.request':
            request_type = 'duplicate_card_request'
        elif model == 'request.for.a.certificate.of.non.royalty':
            request_type = 'request_for_a_certificate_of_non_royalty'
        elif model == 'loan.application':
            request_type = 'loan_application'
        elif model == 'exceptional.loan.application':
            request_type = 'exceptional_loan_application'
        elif model == 'retirement.loan.application':
            request_type = 'retirement_loan_application'
        elif model == 'central.loan.application':
            request_type = 'central_ph6_loan_application'
        elif model == 'evidence.africa.loan.application':
            request_type = 'evidence_africa_loan_application'
        elif model == 'french.bookstore.loan.application':
            request_type = 'french_bookstore_loan_application'
        elif model == 'import.export.loan.application':
            request_type = '3k_import_export_loan_application'
        elif model == 'power.of.attorney.authorization':
            request_type = 'power_of_attorney_authorization'

        if my_requests:
            values.update({
                'my_requests': my_requests,
                'request_type': request_type,
                'model': model,
            })

        return http.request.render('mudci_website.my_requests', values)

    @http.route('/demande/<string:model>/<int:id>/details', auth="user", website=True)
    def request_details(self, model, id, **kw):
        partner = request.env.user.partner_id
        values = {
            'partner': partner,
        }
        
        if not model or not id:
            return request.not_found()

        request_type = ''
        request_details = request.env[model].sudo().search([('id', '=', id)])
        
        if model == 'identification.health.insurance.card':
            request_type = 'identification_health_insurance_card'
        elif model == 'id.photo.change.form':
            request_type = 'id_photo_change_form'
        elif model == 'registration.number.change.form':
            request_type = 'registration_number_change_form'
        elif model == 'name.change.form':
            request_type = 'name_change_form'
        elif model == 'duplicate.card.request':
            request_type = 'duplicate_card_request'
        elif model == 'request.for.a.certificate.of.non.royalty':
            request_type = 'request_for_a_certificate_of_non_royalty'
        elif model == 'loan.application':
            request_type = 'loan_application'
        elif model == 'exceptional.loan.application':
            request_type = 'exceptional_loan_application'
        elif model == 'retirement.loan.application':
            request_type = 'retirement_loan_application'
        elif model == 'central.loan.application':
            request_type = 'central_ph6_loan_application'
        elif model == 'evidence.africa.loan.application':
            request_type = 'evidence_africa_loan_application'
        elif model == 'french.bookstore.loan.application':
            request_type = 'french_bookstore_loan_application'
        elif model == 'import.export.loan.application':
            request_type = '3k_import_export_loan_application'
        elif model == 'power.of.attorney.authorization':
            request_type = 'power_of_attorney_authorization'
        
        if request_details:
            values.update({
                'request_details': request_details,
                'request_type': request_type,
                'model': model,
            })
            
        return http.request.render('mudci_website.request_details', values)
