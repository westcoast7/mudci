# -*- coding: utf-8 -*-

import base64
import json
import logging
import requests
import werkzeug.urls
import werkzeug.utils
from datetime import datetime, timedelta
from pytz import timezone, UTC
from datetime import time

from odoo import _
from odoo import http
from odoo import tools
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.addons.portal.controllers.web import Home
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.exceptions import UserError

from odoo.http import request, route
from odoo.addons.http_routing.models.ir_http import slug, unslug

_logger = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class Home(Home):
    CONTACT_REQUEST_FIELDS = [
        "name",
        "email",
        "phone",
        "subject",
        "message",
    ]

    FAQ_REQUEST_FIELDS = [
        "name",
        "email",
        "phone",
        "ask",
    ]

    FIND_ME_REQUEST_FIELDS = [
        "register_number",
        "code_id",
    ]

    NO_REQUIRED_FIELDS = []

    FILE_UPLOAD_REQUEST_FIELDS = [
        "file",
        "doc_type",
    ]

    def details_form_validate(self, data, request_fields):
        error = dict()
        error_message = []

        # Validation
        for field_name in request_fields:
            if not data.get(field_name):
                error[field_name] = 'missing'

        # error message for empty required fields
        if [err for err in error.values() if err == 'missing']:
            error_message.append(_('Some required fields are empty.'))

        unknown = [k for k in data if k not in request_fields and k not in self.NO_REQUIRED_FIELDS]
        if unknown:
            error['common'] = 'Unknown field'
            error_message.append("Unknown field '%s'" % ','.join(unknown))

        return error, error_message

    @http.route(auth="public")
    def index(self, **kw):
        super(Home, self).index()

        slides = request.env['mudci.slide'].sudo().search([('active', '=', True)], order='id desc')
        newsflash = request.env['mudci.newsflash'].sudo().search([('active', '=', True)], order='id desc')
        mudcinews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=6, order='id desc')
        mobileapp_section = request.env['mudci.homepage.mobileapp.section'].sudo().search([], limit=1, order='id desc')
        mudci_partners = request.env['mudci.partner'].sudo().search([], order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        the_statute_and_rules_of_procedure = request.env['the.statute.and.rules.of.procedure'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')
        health_insurance_specifications = request.env['health.insurance.specifications'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')
        the_mutualists_guide = request.env['the.mutualists.guide'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        rules_of_procedure = request.env['rules.of.procedure'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        mudci_our_products = request.env['mudci.our.products'].sudo().search([('active', '=', True)], order='id desc')
        pca_word = request.env['mudci.pca.word'].sudo().search([], limit=1, order='id desc')
        cmdgic_specialties = request.env['cmdgic.speciality'].sudo().search([], order='id desc')

        values = {
            'slides': slides,
            'newsflash': newsflash,
            'mudcinews': mudcinews,
            'mobileapp_section': mobileapp_section,
            'mudci_partners': mudci_partners,
            'lastnews': lastnews,
            'the_statute_and_rules_of_procedure': the_statute_and_rules_of_procedure,
            'health_insurance_specifications': health_insurance_specifications,
            'the_mutualists_guide': the_mutualists_guide,
            'mudci_our_products': mudci_our_products,
            'rules_of_procedure': rules_of_procedure,
            'pca_word': pca_word,
            'cmdgic_specialties': cmdgic_specialties,
        }

        return request.render('mudci_website.homepage_view', values)

    @http.route('/privacy-policy', auth='public', website=True)
    def privacy_policy(self, **kw):
        privacy_policy_banner = request.env['privacy.policy.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')

        values = {
            'privacy_policy_banner': privacy_policy_banner,
        }

        return http.request.render('mudci_website.privacy_policy', values)

    @http.route('/mot-du-pca', auth='public', website=True)
    def word_from_the_pca(self, **kw):
        pca_word = request.env['mudci.pca.word'].sudo().search([], limit=1, order='id desc')
        pca_word_banner = request.env['pca.word.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                       order='id desc')
        # lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')

        values = {
            'pca_word': pca_word,
            'pca_word_banner': pca_word_banner,
            # 'lastnews': lastnews,
        }

        return http.request.render('mudci_website.word_from_the_pca', values)

    @http.route([
        '''/actualites''',
        '''/actualites/page/<int:page>'''
    ], auth='public', website=True)
    def news(self, page=0, **kw):
        mudcinews_banner = request.env['news.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        news_obj = request.env['mudci.news'].sudo().search([('active', '=', True)])
        total = news_obj.search_count([])

        pager = request.website.pager(
            url='/actualites',
            total=total,
            page=page,
            step=3,
        )

        offset = pager['offset']
        news_obj = news_obj[offset: offset + 3]

        values = {
            'mudcinews': news_obj,
            'mudcinews_banner': mudcinews_banner,
            'categories': categories,
            'lastnews': lastnews,
            'pager': pager,
        }

        return http.request.render('mudci_website.news', values)

    @http.route([
        '''/galerie''',
        '''/galerie/page/<int:page>'''
    ], auth='public', website=True)
    def gallery(self, page=0, **kw):
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        albums_banner = request.env['gallery.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        albums = request.env['mudci.gallery'].sudo().search([('active', '=', True)], order='id desc')

        total = albums.sudo().search_count([])

        pager = request.website.pager(
            url='/galerie',
            total=total,
            page=page,
            step=6,
        )

        offset = pager['offset']
        albums = albums[offset: offset + 6]

        values = {
            'lastnews': lastnews,
            'albums': albums,
            'albums_banner': albums_banner,
            'pager': pager,
        }

        return http.request.render('mudci_website.gallery', values)

    @http.route('/album/<int:album_id>/details', auth='public', website=True)
    def album_details(self, album_id, **kw):
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        albums_banner = request.env['gallery.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        album = request.env['mudci.gallery'].sudo().search([('active', '=', True), ('id', '=', album_id)],
                                                           order='id desc')
        albums = request.env['mudci.gallery'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'lastnews': lastnews,
            'album': album,
            'albums': albums,
            'albums_banner': albums_banner,
            'categories': categories,
        }

        return http.request.render('mudci_website.album_details', values)

    @http.route([
        '''/videotheque''',
        '''/videotheque/page/<int:page>'''
    ], auth='public', website=True)
    def videotech(self, page=0, **kw):
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        videos_banner = request.env['videotec.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')

        videos_obj = request.env['mudci.videotec'].sudo().search([('active', '=', True)], order='id desc')
        total = videos_obj.sudo().search_count([])

        pager = request.website.pager(
            url='/videotheque',
            total=total,
            page=page,
            step=6,
        )

        offset = pager['offset']
        videos_obj = videos_obj[offset: offset + 6]

        values = {
            'lastnews': lastnews,
            'videos': videos_obj,
            'videos_banner': videos_banner,
            'pager': pager,
        }

        return http.request.render('mudci_website.videos', values)

    @http.route('/videotheque/<model("mudci.videotec"):video>/', auth='public', website=True)
    def video_library_details(self, video):
        videos_banner = request.env['videotec.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastvideos = request.env['mudci.videotec'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'video': video,
            'videos_banner': videos_banner,
            'lastvideos': lastvideos,
            'categories': categories,
        }

        return http.request.render('mudci_website.videotec_details', values)

    @http.route('/actualite/<model("mudci.news"):news>/', auth='public', website=True)
    def news_details(self, news):
        mudcinews_banner = request.env['news.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'news': news,
            'mudcinews_banner': mudcinews_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.news_details', values)

    @http.route('/nos-produits/<model("mudci.our.products"):product>/', auth='public', website=True)
    def product_details(self, product):
        product_banner = request.env['product.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'product': product,
            'product_banner': product_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.product_details', values)

    @http.route('/historique', auth='public', website=True)
    def historical(self, **kw):
        historical = request.env['mudci.historical'].sudo().search([], limit=1, order='id desc')
        historical_banner = request.env['mudci.historical.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                 order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'historical': historical,
            'historical_banner': historical_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.historical', values)

    @http.route('/organisation', auth='public', website=True)
    def organisation(self, **kw):
        organisation = request.env['mudci.organisation'].sudo().search([], limit=1, order='id desc')
        organisation_banner = request.env['mudci.organisation.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                     order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'organisation': organisation,
            'organisation_banner': organisation_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.organisation', values)

    @http.route('/beneficiaire', auth='public', website=True)
    def beneficiary(self, **kw):
        beneficiary = request.env['mudci.beneficiary'].sudo().search([], limit=1, order='id desc')
        beneficiary_banner = request.env['mudci.beneficiary.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                   order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'beneficiary': beneficiary,
            'beneficiary_banner': beneficiary_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.beneficiary', values)

    @http.route('/cmdgic/historique', auth='public', website=True)
    def cmdgic_historical(self, **kw):
        cmdgic_historical = request.env['cmdgic.historical'].sudo().search([], limit=1, order='id desc')
        cmdgic_historical_banner = request.env['cmdgic.historical.banner'].sudo().search([('active', '=', True)],
                                                                                         limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'cmdgic_historical': cmdgic_historical,
            'cmdgic_historical_banner': cmdgic_historical_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_historical', values)

    @http.route('/cmdgic/presentation', auth='public', website=True)
    def cmdgic_presentation(self, **kw):
        cmdgic_presentation = request.env['cmdgic.presentation'].sudo().search([], limit=1, order='id desc')
        cmdgic_presentation_banner = request.env['cmdgic.presentation.banner'].sudo().search([('active', '=', True)],
                                                                                             limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'cmdgic_presentation': cmdgic_presentation,
            'cmdgic_presentation_banner': cmdgic_presentation_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_presentation', values)

    @http.route('/cmdgic/parcours-du-client', auth='public', website=True)
    def cmdgic_customer_journey(self, **kw):
        cmdgic_customer_journey = request.env['cmdgic.customer.journey'].sudo().search([], limit=1, order='id desc')
        cmdgic_customer_journey_banner = request.env['cmdgic.customer.journey.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'cmdgic_customer_journey': cmdgic_customer_journey,
            'cmdgic_customer_journey_banner': cmdgic_customer_journey_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.cmdgic_customer_journey', values)

    @http.route('/cmdgic/partenaires-medicaux', auth='public', website=True)
    def cmdgic_medical_partners(self, **kw):
        doctor_programs_banner = request.env['cmdgic.medical.partners.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')

        values = {
            'doctor_programs_banner': doctor_programs_banner
        }

        return http.request.render('mudci_website.doctor_programs', values)

    @http.route('/nos-partenaires', auth='public', website=True)
    def mudci_partners(self, **kw):
        mudci_partners = request.env['mudci.partner'].sudo().search([], order='id desc')
        mudci_partners_banner = request.env['partner.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                            order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'mudci_partners': mudci_partners,
            'mudci_partners_banner': mudci_partners_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.mudci_partners', values)

    @http.route('/faq', auth='public', website=True)
    def faq(self, **post):
        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            mudci_portal_user_question = request.env['mudci.portal.user.question']
            error, error_message = self.details_form_validate(post, self.FAQ_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.FAQ_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    mudci_portal_user_question_obj = mudci_portal_user_question.sudo().create({
                        'name': values['name'],
                        'email': values['email'],
                        'phone': values['phone'],
                        'ask': values['ask'],
                    })

                    template_rec = request.env.ref('mudci_backend.email_template_notify_portal_user_question').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(mudci_portal_user_question_obj.id, force_send=True)

                    success_message = [_('Message has sent successfully.')]
                    values.update({'success_message': success_message})

                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        mudci_faq = request.env['mudci.faq'].sudo().search([('active', '=', True)], order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        mudci_faq_banner = request.env['faq.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')

        values.update({'lastnews': lastnews, 'mudci_faq': mudci_faq, 'mudci_faq_banner': mudci_faq_banner})

        return http.request.render('mudci_website.faq', values)

    @http.route(['/contactus', '/contactus/<string:mode>'], auth='public', website=True, csrf=False)
    def contactus(self, mode=None, **post):
        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            mudci_contact = request.env['mudci.contact']
            error, error_message = self.details_form_validate(post, self.CONTACT_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                values = {key: post[key] for key in self.CONTACT_REQUEST_FIELDS}
                values.update({'error': {}, 'error_message': [], })

                try:
                    mudci_contact_obj = mudci_contact.sudo().create({
                        'name': values['name'],
                        'email': values['email'],
                        'phone': values['phone'],
                        'subject': values['subject'],
                        'message': values['message'],
                    })

                    template_rec = request.env.ref(
                        'mudci_backend.email_template_notify_website_contact_form_message').sudo()
                    template_rec.write({'email_to': request.env.company.email, 'email_from': tools.formataddr((request.env.company.name, request.env.company.email))})
                    template_rec.send_mail(mudci_contact_obj.id, force_send=True)

                    success_message = [_('Message has sent successfully.')]
                    values.update({'success_message': success_message})

                    if mode == 'ajax':
                        return json.dumps(success_message)

                except Exception as e:
                    _logger.exception(e)
                    error_message = [_(f"{type(e).__name__} was raised: {e}")]
                    values.update({'error_message': error_message})

        last_news = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        values.update({'lastnews': last_news})

        return http.request.render('mudci_website.contactus', values)

    @http.route('/prestations-sociales', auth='public', website=True)
    def social_security_benefits(self, **kw):
        social_security_benefits = request.env['mudci.social.security.benefit'].sudo().search([], limit=1,
                                                                                              order='id desc')
        social_security_benefits_banner = request.env['social.security.benefits.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'categories': categories,
            'social_security_benefits': social_security_benefits,
            'social_security_benefits_banner': social_security_benefits_banner,
            'lastnews': lastnews,
        }

        return http.request.render('mudci_website.social_security_benefits', values)

    @http.route([
        '''/categorie/<model("mudci.news.category"):category>/actualites/''',
        '''/categorie/<model("mudci.news.category"):category>/actualites/page/<int:page>'''
    ], auth='public', website=True)
    def category_news(self, category, page=0):
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        mudcinews_banner = request.env['news.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        search_news = request.env['mudci.news'].sudo().search(
            [('active', '=', True), ('category_id', '=', category.id)])
        total = len(search_news)

        pager = request.website.pager(
            url='/categorie/' + slug(category) + '/actualites/',
            total=total,
            page=page,
            step=3,
        )

        offset = pager['offset']
        category_news = search_news[offset: offset + 3]

        values = {
            'category_news': category_news,
            'mudcinews_banner': mudcinews_banner,
            'lastnews': lastnews,
            'categories': categories,
            'pager': pager,
        }

        return http.request.render('mudci_website.category_news', values)

    @http.route([
        '''/categorie/<model("mudci.news.category"):category>/albums/''',
        '''/categorie/<model("mudci.news.category"):category>/albums/page/<int:page>'''
    ], auth='public', website=True)
    def category_albums(self, category, page=0):
        albums_banner = request.env['gallery.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        search_albums = request.env['mudci.gallery'].sudo().search(
            [('active', '=', True), ('category_id', '=', category.id)])
        total = len(search_albums)

        pager = request.website.pager(
            url='/categorie/' + slug(category) + '/albums/',
            total=total,
            page=page,
            step=6,
        )

        offset = pager['offset']
        category_albums = search_albums[offset: offset + 6]

        values = {
            'category_albums': category_albums,
            'albums_banner': albums_banner,
            'categories': categories,
            'pager': pager,
        }

        return http.request.render('mudci_website.category_albums', values)

    @http.route([
        '''/categorie/<model("mudci.news.category"):category>/videos/''',
        '''/categorie/<model("mudci.news.category"):category>/videos/page/<int:page>'''
    ], auth='public', website=True)
    def category_videos(self, category, page=0):
        videos_banner = request.env['gallery.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        search_videos = request.env['mudci.videotec'].sudo().search(
            [('active', '=', True), ('category_id', '=', category.id)])
        total = len(search_videos)

        pager = request.website.pager(
            url='/categorie/' + slug(category) + '/videos/',
            total=total,
            page=page,
            step=6,
        )

        offset = pager['offset']
        category_videos = search_videos[offset: offset + 6]

        values = {
            'category_videos': category_videos,
            'videos_banner': videos_banner,
            'categories': categories,
            'pager': pager,
        }

        return http.request.render('mudci_website.category_videos', values)

    @http.route('/web/find-me', auth='public', website=True)
    def find_me(self, **post):
        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        values = {
            'error': {},
            'error_message': [],
            'success_message': [],
        }

        if post and request.httprequest.method == 'POST':
            res_mutualist_obj = request.env['res.mutualist']
            res_users_obj = request.env['res.users'].sudo()
            check_user_exist = res_users_obj.search([('login', '=', post['register_number'])])
            error, error_message = self.details_form_validate(post, self.FIND_ME_REQUEST_FIELDS)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)

            if not error:
                if not check_user_exist:
                    values = {key: post[key] for key in self.FIND_ME_REQUEST_FIELDS}
                    values.update({'error': {}, 'error_message': [], })

                    try:
                        mutualist_record = res_mutualist_obj.sudo().search(
                            [('code_id', '=', values['code_id']), ('register_number', '=', values['register_number'])])

                        if mutualist_record:
                            return werkzeug.utils.redirect('/web/signup?register_number=%s&code_id=%s' % (
                                mutualist_record.register_number, mutualist_record.code_id))
                        else:
                            error_message = [_("Informations non trouvées.")]
                            values.update({'error_message': error_message})
                    except Exception as e:
                        _logger.exception(e)
                        error_message = [_(f"{type(e).__name__} was raised: {e}")]
                        values.update({'error_message': error_message})
                else:
                    url = "/web/login"
                    error_message = [_('Vous avez déjà un compte. <br/><b><a href="%s">Connectez-vous</a></b>') % (url)]
                    values.update({'error_message': error_message})

        values.update({'lastnews': lastnews})

        return http.request.render('mudci_website.find_me', values)

    @http.route('/upload/profile/document', auth='user', website=True, csrf=False)
    def upload_profile_document(self, **post):
        if post and request.httprequest.method == 'POST':
            if post.get('file', False) and post.get('doc_type'):
                doc_type = post.get('doc_type')
                partner = request.env.user.partner_id
                file = request.httprequest.files.getlist('file')[0]
                error, error_message = self.details_form_validate(post, self.FILE_UPLOAD_REQUEST_FIELDS)

                if not error:
                    if file.filename != '':
                        if allowed_file(file.filename):
                            try:
                                if doc_type == 'id_photo':
                                    partner.sudo().write({
                                        'id_photo': base64.encodebytes(file.read()),
                                        'id_photo_name': file.filename
                                    })

                                elif doc_type == 'cni':
                                    partner.sudo().write({
                                        'cni': base64.encodebytes(file.read()),
                                        'cni_name': file.filename
                                    })

                                elif doc_type == 'dgd_assignment_decision':
                                    partner.sudo().write({
                                        'dgd_assignment_decision': base64.encodebytes(file.read()),
                                        'dgd_assignment_decision_name': file.filename
                                    })

                                elif doc_type == 'service_start_report':
                                    partner.sudo().write({
                                        'service_start_report': base64.encodebytes(file.read()),
                                        'service_start_report_name': file.filename
                                    })

                                elif doc_type == 'mudci_access_card':
                                    partner.sudo().write({
                                        'mudci_access_card': base64.encodebytes(file.read()),
                                        'mudci_access_card_name': file.filename
                                    })
                                else:
                                    error_message = [_('Impossible d\'enregistrer le fichier.')]
                                    return json.dumps({"error_message": error_message})

                                success_message = [_('Fichier enregistré avec succès.')]
                                return json.dumps({"success_message": success_message})

                            except Exception as e:
                                _logger.exception(e)
                                return json.dumps({'error_message': str(e)})
                        else:
                            error_message = [_('Fichier non autorisé.')]
                            return json.dumps({"error_message": error_message})
                    else:
                        error_message = [_('Aucun fichier sélectionné.')]
                        return json.dumps({"error_message": error_message})
                else:
                    return json.dumps({"error_message": error_message})

    @http.route('/prestations-sociales/presentation', auth='public', website=True)
    def social_benefits_presentation(self, **kw):
        social_benefits_presentation = request.env['ssb.presentation'].sudo().search([], limit=1, order='id desc')
        social_benefits_presentation_banner = request.env['ssb.presentation.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'social_benefits_presentation': social_benefits_presentation,
            'social_benefits_presentation_banner': social_benefits_presentation_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.social_benefits_presentation', values)

    @http.route('/prestations-sociales/etablissement-carte-mudci', auth='public', website=True)
    def social_benefits_establishment_mudci_card(self, **kw):
        social_benefits_establishment_mudci_card = request.env['ssb.establishing.mudci.card'].sudo().search([], limit=1,
                                                                                                            order='id desc')
        social_benefits_establishment_mudci_card_banner = request.env[
            'ssb.establishing.mudci.card.banner'].sudo().search([('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'social_benefits_establishment_mudci_card': social_benefits_establishment_mudci_card,
            'social_benefits_establishment_mudci_card_banner': social_benefits_establishment_mudci_card_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.social_benefits_establishment_mudci_card', values)

    @http.route('/prestations-sociales/fond-de-solidarite', auth='public', website=True)
    def social_benefits_solidarity_fund(self, **kw):
        social_benefits_solidarity_fund = request.env['ssb.solidarity.fund'].sudo().search([], limit=1, order='id desc')
        social_benefits_solidarity_fund_banner = request.env['ssb.solidarity.fund.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'social_benefits_solidarity_fund': social_benefits_solidarity_fund,
            'social_benefits_solidarity_fund_banner': social_benefits_solidarity_fund_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.social_benefits_solidarity_fund', values)

    @http.route('/prestations-sociales/fond-de-financement-des-projets-communs-de-developpement', auth='public',
                website=True)
    def social_benefits_fund_for_financing_common_development_projects(self, **kw):
        social_benefits_fund_for_financing_common_development_projects = request.env[
            'ssb.fund.for.financing.joint.development.projects'].sudo().search([], limit=1, order='id desc')
        social_benefits_fund_for_financing_common_development_projects_banner = request.env[
            'ssb.fund.for.financing.joint.development.projects.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'social_benefits_fund_for_financing_common_development_projects': social_benefits_fund_for_financing_common_development_projects,
            'social_benefits_fund_for_financing_common_development_projects_banner': social_benefits_fund_for_financing_common_development_projects_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.social_benefits_fund_for_financing_common_development_projects',
                                   values)

    @http.route('/prestations-sociales/assurance-vie-et-assistance-frais-funeraires', auth='public',
                website=True)
    def social_benefits_life_insurance_and_funeral_expenses_assistance(self, **kw):
        social_benefits_life_insurance_and_funeral_expenses_assistance = request.env[
            'ssb.life.insurance.and.funeral.expenses.insurance'].sudo().search([], limit=1, order='id desc')
        social_benefits_life_insurance_and_funeral_expenses_assistance_banner = request.env[
            'ssb.life.insurance.and.funeral.expenses.insurance.banner'].sudo().search([('active', '=', True)], limit=1,
                                                                                      order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'social_benefits_life_insurance_and_funeral_expenses_assistance': social_benefits_life_insurance_and_funeral_expenses_assistance,
            'social_benefits_life_insurance_and_funeral_expenses_assistance_banner': social_benefits_life_insurance_and_funeral_expenses_assistance_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.social_benefits_life_insurance_and_funeral_expenses_assistance',
                                   values)

    @http.route('/prestations-sociales/assurance-flotte-automobile', auth='public', website=True)
    def social_benefits_car_fleet_insurance(self, **kw):
        social_benefits_car_fleet_insurance = request.env[
            'ssb.motor.fleet.insurance'].sudo().search([], limit=1, order='id desc')
        social_benefits_car_fleet_insurance_banner = request.env[
            'ssb.motor.fleet.insurance.banner'].sudo().search([('active', '=', True)], limit=1,
                                                              order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'social_benefits_car_fleet_insurance': social_benefits_car_fleet_insurance,
            'social_benefits_car_fleet_insurance_banner': social_benefits_car_fleet_insurance_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.social_benefits_car_fleet_insurance', values)

    @http.route('/assurance-maladie/presentation', auth='public', website=True)
    def health_insurance_presentation(self, **kw):
        health_insurance_presentation = request.env[
            'hi.presentation'].sudo().search([], limit=1, order='id desc')
        health_insurance_presentation_banner = request.env[
            'hi.presentation.banner'].sudo().search([('active', '=', True)], limit=1,
                                                    order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'health_insurance_presentation': health_insurance_presentation,
            'health_insurance_presentation_banner': health_insurance_presentation_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.health_insurance_presentation', values)

    @http.route('/assurance-maladie/principales-garanties', auth='public', website=True)
    def health_insurance_main_guarantees(self, **kw):
        health_insurance_main_guarantees = request.env[
            'hi.main.guarantees'].sudo().search([], limit=1, order='id desc')
        health_insurance_main_guarantees_banner = request.env[
            'hi.main.guarantees.banner'].sudo().search([('active', '=', True)], limit=1,
                                                       order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'health_insurance_main_guarantees': health_insurance_main_guarantees,
            'health_insurance_main_guarantees_banner': health_insurance_main_guarantees_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.health_insurance_main_guarantees', values)

    @http.route('/assurance-maladie/principales-exclusions', auth='public', website=True)
    def health_insurance_main_exclusions(self, **kw):
        health_insurance_main_exclusions = request.env['hi.main.exclusions'].sudo().search([], limit=1, order='id desc')
        health_insurance_main_exclusions_banner = request.env['hi.main.exclusions.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'health_insurance_main_exclusions': health_insurance_main_exclusions,
            'health_insurance_main_exclusions_banner': health_insurance_main_exclusions_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.health_insurance_main_exclusions', values)

    @http.route('/assurance-maladie/procedure-de-remboursement', auth='public', website=True)
    def health_insurance_refund_procedure(self, **kw):
        health_insurance_refund_procedure = request.env['hi.refund.procedure'].sudo().search([], limit=1,
                                                                                             order='id desc')
        health_insurance_refund_procedure_banner = request.env['hi.refund.procedure.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'health_insurance_refund_procedure': health_insurance_refund_procedure,
            'health_insurance_refund_procedure_banner': health_insurance_refund_procedure_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.health_insurance_refund_procedure', values)

    @http.route('/cmdgic/plateau-technique', auth='public', website=True)
    def cmdgic_technical_platform(self, **kw):
        cmdgic_technical_platform = request.env['cmdgic.technical.platform'].sudo().search([], limit=1, order='id desc')
        cmdgic_technical_platform_banner = request.env['cmdgic.technical.platform.banner'].sudo().search(
            [('active', '=', True)], limit=1, order='id desc')

        lastnews = request.env['mudci.news'].sudo().search([('active', '=', True)], limit=5, order='id desc')
        categories = request.env['mudci.news.category'].sudo().search([], order='id desc')

        values = {
            'cmdgic_technical_platform': cmdgic_technical_platform,
            'cmdgic_technical_platform_banner': cmdgic_technical_platform_banner,
            'lastnews': lastnews,
            'categories': categories,
        }

        return http.request.render('mudci_website.cmdgic_technical_platform', values)


def save_attachment(attachment, partner, filename, document_type):
    attachment_obj = request.env['ir.attachment'].sudo()

    if attachment:
        attachment_value = {
            'name': filename,
            'datas': base64.encodebytes(attachment),
            'res_model': 'res.partner',
            'res_id': partner.id,
            'document_type': document_type,
        }
        return attachment_obj.create(attachment_value)


class ExtensionAuthSignMain(AuthSignupHome):

    @http.route()
    def web_auth_signup(self, *args, **kw):
        response = super(ExtensionAuthSignMain, self).web_auth_signup(*args, **kw)
        response.qcontext.update(self.get_auth_signup_qcontext())

        if not kw.get('code_id') and not kw.get('register_number'):
            return werkzeug.utils.redirect('/web/find-me')

        mutualist_record = request.env['res.mutualist'].sudo().search(
            [('code_id', '=', kw.get('code_id')), ('register_number', '=', kw.get('register_number'))])

        if mutualist_record:
            response.qcontext.update({
                'mutualist_record': mutualist_record,
            })
        else:
            return werkzeug.utils.redirect('/web/find-me')

        if kw.get('login'):
            partner_record = request.env['res.partner'].sudo().search([('email', '=', kw.get('login'))], limit=1)

            if partner_record:
                if kw.get('id_photo', False):
                    document_type = 'id_photo'
                    id_photo_name = kw.get('id_photo').filename
                    file = kw.get('id_photo')
                    id_photo = file.read()
                    # save_attachment(id_photo, partner_record, name, document_type=document_type)
                    partner_record.write({
                        'id_photo': id_photo,
                        'id_photo_name': id_photo_name
                    })

                if kw.get('cni', False):
                    document_type = 'cni'
                    name = kw.get('cni').filename
                    file = kw.get('cni')
                    cni = file.read()
                    # save_attachment(cni, partner_record, name, document_type=document_type)
                    partner_record.write({
                        'cni': cni,
                        'cni_name': cni_name
                    })

                if kw.get('dgd_assignment_decision', False):
                    document_type = 'dgd_assignment_decision'
                    dgd_assignment_decision_name = kw.get('dgd_assignment_decision').filename
                    file = kw.get('dgd_assignment_decision')
                    dgd_assignment_decision = file.read()
                    # save_attachment(dgd_assignment_decision, partner_record, name, document_type=document_type)
                    partner_record.write({
                        'dgd_assignment_decision': dgd_assignment_decision,
                        'dgd_assignment_decision_name': dgd_assignment_decision_name
                    })

                if kw.get('service_start_report', False):
                    document_type = 'service_start_report'
                    service_start_report_name = kw.get('service_start_report').filename
                    file = kw.get('service_start_report')
                    service_start_report = file.read()
                    # save_attachment(service_start_report, partner_record, name, document_type=document_type)
                    partner_record.write({
                        'service_start_report': service_start_report,
                        'service_start_report_name': service_start_report_name
                    })

                if kw.get('mudci_access_card', False):
                    document_type = 'mudci_access_card'
                    mudci_access_card_name = kw.get('mudci_access_card').filename
                    file = kw.get('mudci_access_card')
                    mudci_access_card = file.read()
                    # save_attachment(mudci_access_card, partner_record, name, document_type=document_type)
                    partner_record.write({
                        'mudci_access_card': mudci_access_card,
                        'mudci_access_card_name': mudci_access_card_name
                    })

                partner_record.write({
                    'last_name': kw.get('last_name'),
                    'first_name': kw.get('first_name'),
                    'direction': kw.get('direction'),
                    'service': kw.get('service'),
                    'job_title': kw.get('job_title'),
                    'function': kw.get('function'),
                    'work_location': kw.get('work_location'),
                    'register_number': kw.get('login'),
                    'code_id': mutualist_record.code_id,
                    'birthday': kw.get('birthday'),
                    'gender': kw.get('gender'),
                    'marital': kw.get('marital'),
                    'mobile': kw.get('mobile_phone'),
                    'phone': kw.get('work_phone'),
                    'email': kw.get('work_email'),
                    'is_mutualist': True,
                })

        return response


class ExtensionCustomerPortal(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["register_number", "last_name", "first_name", "birthday", "gender", "marital",
                                "mobile", "email", "direction", "service", "work_location", "function",
                                "job_title"]
    OPTIONAL_BILLING_FIELDS = ["phone"]

    def _prepare_portal_layout_values(self):
        res = super(ExtensionCustomerPortal, self)._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        res['partner'] = partner
        return res
