# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class PcaWordBanner(models.Model):
    """ Pca word banner. """

    _name = "pca.word.banner"
    _description = "Pca word banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MudciHistoricalBanner(models.Model):
    """ Mudci historical banner. """

    _name = "mudci.historical.banner"
    _description = "Mudci historical banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MudciOrganisationBanner(models.Model):
    """ Mudci organisation banner. """

    _name = "mudci.organisation.banner"
    _description = "Mudci organisation banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MudciBeneficiaryBanner(models.Model):
    """ Mudci beneficiary banner. """

    _name = "mudci.beneficiary.banner"
    _description = "Mudci beneficiary banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class SocialSecurityBenefitsBanner(models.Model):
    """ social security benefits banner. """

    _name = "social.security.benefits.banner"
    _description = "social security benefits banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CmdgicHistoricalBanner(models.Model):
    """ Cmdgic historical banner. """

    _name = "cmdgic.historical.banner"
    _description = "Cmdgic historical banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CmdgicPresentationBanner(models.Model):
    """ Cmdgic presentation banner. """

    _name = "cmdgic.presentation.banner"
    _description = "Cmdgic presentation banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CmdgicCustomerJourneyBanner(models.Model):
    """ Cmdgic customer journey banner. """

    _name = "cmdgic.customer.journey.banner"
    _description = "Cmdgic customer journey banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CmdgicSpecialtyBanner(models.Model):
    """ Cmdgic specialty banner. """

    _name = "cmdgic.specialty.banner"
    _description = "Cmdgic specialty banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CmdgicProgramBanner(models.Model):
    """ Cmdgic program banner. """

    _name = "cmdgic.program.banner"
    _description = "Cmdgic program banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CmdgicMedicalPartnersBanner(models.Model):
    """ Cmdgic medical partners banner. """

    _name = "cmdgic.medical.partners.banner"
    _description = "Cmdgic medical partners banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class NewsBanner(models.Model):
    """ News banner. """

    _name = "news.banner"
    _description = "News banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class GalleryBanner(models.Model):
    """ Gallery banner. """

    _name = "gallery.banner"
    _description = "Gallery banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class VideotecBanner(models.Model):
    """ Videotec banner. """

    _name = "videotec.banner"
    _description = "Videotec banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class FaqBanner(models.Model):
    """ Faq banner. """

    _name = "faq.banner"
    _description = "Faq banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class PartnerBanner(models.Model):
    """ Partner banner. """

    _name = "partner.banner"
    _description = "Partner banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class DoctorProgramBanner(models.Model):
    """ Doctor program banner. """

    _name = "doctor.program.banner"
    _description = "Doctor program banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class OpticalCenterBanner(models.Model):
    """ Optical Center Banner. """

    _name = "optical.center.banner"
    _description = "Optical Center Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class PharmacyBanner(models.Model):
    """ Pharmacy Banner. """

    _name = "pharmacy.banner"
    _description = "Pharmacy Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MedicalExamBanner(models.Model):
    """ Medical Exam Banner. """

    _name = "medical.exam.banner"
    _description = "Medical Exam Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class OurResourcesBanner(models.Model):
    """ Our Resources Banner. """

    _name = "our.resources.banner"
    _description = "Our resources Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class SsbPresentationBanner(models.Model):
    """ Ssb Presentation Banner. """

    _name = "ssb.presentation.banner"
    _description = "Ssb Presentation Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class EstablishingMudciCardBanner(models.Model):
    """ Establishing Mudci Card Banner. """

    _name = "ssb.establishing.mudci.card.banner"
    _description = "Establishing Mudci Card Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class SolidarityFundBanner(models.Model):
    """ Solidarity Fund Banner. """

    _name = "ssb.solidarity.fund.banner"
    _description = "Solidarity Fund Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class FundForFinancingJointDevelopmentProjectsBanner(models.Model):
    """ Fund For Financing Joint Development Projects Banner. """

    _name = "ssb.fund.for.financing.joint.development.projects.banner"
    _description = "Fund For Financing Joint Development Projects Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class LifeInsuranceAndFuneralExpensesInsuranceBanner(models.Model):
    """ Life insurance and funeral expenses insurance Banner. """

    _name = "ssb.life.insurance.and.funeral.expenses.insurance.banner"
    _description = "Life insurance and funeral expenses insurance Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MotorFleetInsuranceBanner(models.Model):
    """ Motor fleet insurance Banner. """

    _name = "ssb.motor.fleet.insurance.banner"
    _description = "Motor fleet insurance Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class HealthInsurancePresentationBanner(models.Model):
    """ Health Insurance Presentation Banner. """

    _name = "hi.presentation.banner"
    _description = "Health Insurance Presentation Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MainGuaranteesBanner(models.Model):
    """ Main guarantees Banner. """

    _name = "hi.main.guarantees.banner"
    _description = "Main guarantees Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MainExclusionsBanner(models.Model):
    """ Main exclusions Banner. """

    _name = "hi.main.exclusions.banner"
    _description = "Main exclusions Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class RefundProcedureBanner(models.Model):
    """ Refund Procedure Banner. """

    _name = "hi.refund.procedure.banner"
    _description = "Refund Procedure Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class TechnicalPlatformBanner(models.Model):
    """ Technical platform Banner. """

    _name = "cmdgic.technical.platform.banner"
    _description = "Technical platform Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class PrivacyPolicyBanner(models.Model):
    """ Privacy Policy Banner. """

    _name = "privacy.policy.banner"
    _description = "Privacy Policy Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class CareNetworkBanner(models.Model):
    """ Care Network Banner. """

    _name = "care.network.banner"
    _description = "Care Network Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Titre', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Telechargé par', default=lambda self: self.env.uid)


class OurProductBanner(models.Model):
    """ Product Banner. """

    _name = "product.banner"
    _description = "Product Banner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Titre', required=True, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Telechargé par', default=lambda self: self.env.uid)
