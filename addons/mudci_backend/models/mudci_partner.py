# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class MudciPartner(models.Model):
    """ Mudci Beneficiary. """

    _name = 'mudci.partner'
    _description = "MUDCI Partner"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _inherits = {
        'res.partner': 'partner_id',
    }
    _order = "id desc"

    partner_id = fields.Many2one('res.partner', string='Related partner', required=True, ondelete='cascade')
