# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import slug

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class MudciMediaCategory(models.Model):
    """ Mudci Media Category model. """

    _name = "mudci.media.category"
    _description = "MUDCI Media Category"
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    gallery_ids = fields.One2many('mudci.gallery', 'category_id', string='galleries')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)


class MudciGallery(models.Model):
    """ Mudci Gallery model.

    This model is used to add Gallery.

    """

    _name = "mudci.gallery"
    _description = "MUDCI Gallery"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)
    attachment_ids = fields.One2many('mudci.gallery.attachment', 'gallery_id', string='galleries')
    category_id = fields.Many2one('mudci.news.category', string='Category')
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    date_published = fields.Datetime('Publish Date', readonly=False, tracking=1)
    slug = fields.Char(string='Slug', compute='_compute_slug', store=True)

    def _compute_slug(self):
        for rec in self:
            rec.slug = slug(rec)


class IrAttachment(models.Model):
    """ Extension of ir.attachment.
    """

    _name = 'mudci.gallery.attachment'
    _description = "MUDCI Gallery Attachment"
    _inherits = {
        'ir.attachment': 'ir_attachment_id',
    }
    _order = "id desc"

    ir_attachment_id = fields.Many2one('ir.attachment', string='Related attachment', required=True, ondelete='cascade')
    gallery_id = fields.Many2one('mudci.gallery', string='Gallery')


class MudciVideotec(models.Model):
    """ Mudci videotec model.

    This model is used to add Gallery.

    """

    _name = "mudci.videotec"
    _description = "MUDCI Videotec"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    video_link = fields.Char(string='Video link')
    category_id = fields.Many2one('mudci.news.category', string='Category')
    active = fields.Boolean(default=True, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    date_published = fields.Datetime('Publish Date', readonly=False, tracking=1)
    slug = fields.Char(string='Slug', compute='_compute_slug', store=True)

    def _compute_slug(self):
        for rec in self:
            rec.slug = slug(rec)
