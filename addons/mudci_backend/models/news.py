# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import slug

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class MudciNews(models.Model):
    """ Mudci News model.

    This model is used to add News.

    """

    _name = "mudci.news"
    _description = "MUDCI News"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    short_description = fields.Html(string='Courte description')
    short_name = fields.Char(string='Short name', compute='_compute_short_name')
    date_published = fields.Datetime('Publish Date', readonly=False, tracking=1)
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)
    image_80 = fields.Image("Image 80", related="image_1920", max_width=80, max_height=80, store=True)
    active = fields.Boolean(default=True, tracking=1)
    category_id = fields.Many2one('mudci.news.category', string='Category')
    slug = fields.Char(string='Slug', compute='_compute_slug', store=True)
    
    def _compute_slug(self):
        for rec in self:
            rec.slug = slug(rec)
                
    @api.depends('name')
    @api.onchange('name')
    def _compute_short_name(self):
        for rec in self:
            if rec.name:
                rec.short_name = (rec.name[:25] + '..') if len(rec.name) > 25 else rec.name


class MudciNewsCategory(models.Model):
    """ Mudci News Category model. """

    _name = "mudci.news.category"
    _description = "MUDCI News Category"
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    news_ids = fields.One2many('mudci.news', 'category_id', string='Catgory news')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
