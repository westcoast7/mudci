# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class Direction(models.Model):
    _name = 'mudci.direction'
    _inherit = "hr.department"
    _description = "Direction"
    _order = "name"
    _rec_name = 'name'

    service_ids = fields.One2many('mudci.service', 'direction_id', string='Services')


class Service(models.Model):
    _name = 'mudci.service'
    _inherit = "hr.department"
    _description = "Service"
    _order = "name"
    _rec_name = 'name'

    direction_id = fields.Many2one('mudci.direction', 'Direction')
