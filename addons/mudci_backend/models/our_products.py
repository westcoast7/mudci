# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import base64
from datetime import datetime, timedelta
import io
import re
import requests
import PyPDF2
import json

from dateutil.relativedelta import relativedelta
from PIL import Image
from werkzeug import urls

from odoo import api, fields, models, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.exceptions import UserError, AccessError
from odoo.http import request
from odoo.addons.http_routing.models.ir_http import url_for
from odoo.tools import sql

_logger = logging.getLogger(__name__)


class OurProducts(models.Model):
    """
        Mudci our products model.
    """
    _name = 'mudci.our.products'
    _description = "MUDCI Our products"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char('Title', required=False, translate=True)
    active = fields.Boolean(default=True, tracking=1)
    description = fields.Html(string='Description')
    link = fields.Char(string='Link')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    