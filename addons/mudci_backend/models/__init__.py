# -*- coding: utf-8 -*-

from . import slide
from . import newsflash
from . import news
from . import main
from . import mudci_partner
from . import cmdgic
from . import res_company
from . import mudci_contact
from . import media
from . import banner
from . import social_security_benefits
from . import health_insurance
from . import documents_to_download
from . import our_products
