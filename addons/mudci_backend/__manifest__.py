# -*- coding: utf-8 -*-
{
    'name': "mudci_backend",

    'summary': """
        MUDCI Backend""",

    'description': """
        MUDCI Backend
    """,

    'author': "Pierre Alain KOUAKOU",
    'website': "http://www.douanes.ci/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Management',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'website'],

    # always loaded
    'data': [
        'security/mudci_backend_security.xml',
        'security/ir.model.access.csv',
        'views/mudci_menus.xml',
        'views/mudci_slides_views.xml',
        'views/mudci_newsflash_views.xml',
        'views/mudci_news_views.xml',
        'views/mudci_news_categories_views.xml',
        'views/mudci_homepage_mobileapp_section_views.xml',
        'views/mudci_pca_word_views.xml',
        'views/mudci_historical_views.xml',
        # 'views/mudci_social_security_benefit.xml',
        'views/mudci_organisation_views.xml',
        'views/mudci_beneficiary_views.xml',
        'views/mudci_partner_views.xml',
        'views/mudci_faq_views.xml',
        'views/mudci_portal_user_question_views.xml',
        'views/mudci_contact_views.xml',
        'views/mudci_gallery_views.xml',
        'views/mudci_useful_links_views.xml',
        'views/cmdgic_historical_views.xml',
        'views/cmdgic_presentation_views.xml',
        'views/cmdgic_customer_journey_views.xml',
        'views/cmdgic_speciality_views.xml',
        'views/cmdgic_program_views.xml',
        'views/cmdgic_medical_partners_views.xml',
        'views/mudci_company_views.xml',
        'views/pca_word_banner_views.xml',
        'views/mudci_historical_banner_views.xml',
        'views/mudci_organisation_banner_views.xml',
        'views/mudci_beneficiary_banner_views.xml',
        'views/social_security_benefits_banner_views.xml',
        'views/cmdgic_historical_banner_views.xml',
        'views/cmdgic_presentation_banner_views.xml',
        'views/cmdgic_customer_journey_banner_views.xml',
        'views/cmdgic_specialty_banner_views.xml',
        'views/cmdgic_program_banner_views.xml',
        'views/cmdgic_medical_partners_banner_views.xml',
        'views/cmdgic_technical_platform.xml',
        'views/optical_center_banner_views.xml',
        'views/pharmacy_banner_views.xml',
        'views/medical_exam_banner_views.xml',
        'views/news_banner_views.xml',
        'views/gallery_banner_views.xml',
        'views/videotec_banner_views.xml',
        'views/faq_banner_views.xml',
        'views/partner_banner_views.xml',
        'views/doctor_program_banner_views.xml',
        'views/our_resources_banner_views.xml',
        'views/mudci_videotec_views.xml',
        'views/social_security_benefits/ssb_presentation.xml',
        'views/social_security_benefits/ssb_establishing_mudci_card.xml',
        'views/social_security_benefits/ssb_solidarity_fund.xml',
        'views/social_security_benefits/ssb_fund_for_financing_joint_development_projects.xml',
        'views/social_security_benefits/ssb_life_insurance_and_funeral_expenses_insurance.xml',
        'views/social_security_benefits/ssb_motor_fleet_insurance.xml',
        'views/health_insurance/hi_presentation.xml',
        'views/health_insurance/hi_main_guarantees.xml',
        'views/health_insurance/hi_main_exclusions.xml',
        'views/health_insurance/hi_refund_procedure.xml',
        'views/banners/ssb_presentation_banner.xml',
        'views/banners/ssb_establishing_mudci_card_banner.xml',
        'views/banners/ssb_solidarity_fund_banner.xml',
        'views/banners/ssb_fund_for_financing_joint_development_projects_banner.xml',
        'views/banners/ssb_life_insurance_and_funeral_expenses_insurance_banner.xml',
        'views/banners/ssb_motor_fleet_insurance_banner.xml',
        'views/banners/hi_presentation_banner.xml',
        'views/banners/hi_main_guarantees_banner.xml',
        'views/banners/hi_main_exclusions_banner.xml',
        'views/banners/hi_refund_procedure_banner.xml',
        'views/banners/cmdgic_technical_platform_banner.xml',
        'views/banners/privacy_policy_banner_views.xml',
        'views/banners/care_network_banner_views.xml',
        'views/banners/product_banner_views.xml',
        'views/documents_to_download/the_statute_and_rules_of_procedure_views.xml',
        'views/documents_to_download/health_insurance_specifications_views.xml',
        'views/documents_to_download/the_mutualists_guide_views.xml',
        'views/documents_to_download/rules_of_procedure_views.xml',
        'views/mudci_our_products_views.xml',
        'data/ir_cron_data.xml',
        'data/email_template_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
