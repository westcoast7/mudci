# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request

from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class HealthInsurancePresentation(models.Model):
    """ Health Insurance Presentation. """

    _name = "hi.presentation"
    _description = "Health Insurance Presentation"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class MainGuarantees(models.Model):
    """ Main guarantees. """

    _name = "hi.main.guarantees"
    _description = "Main guarantees"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class MainExclusions(models.Model):
    """ Main guarantees. """

    _name = "hi.main.exclusions"
    _description = "Main exclusions"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)


class RefundProcedure(models.Model):
    """ Refund Procedure. """

    _name = "hi.refund.procedure"
    _description = "Refund Procedure"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'image.mixin']
    _order = 'id desc'

    name = fields.Char(string='Name')
    description = fields.Html(string='Description')
    user_id = fields.Many2one('res.users', string='Uploaded by', default=lambda self: self.env.uid)
    image_1200 = fields.Image("Image 1200", related="image_1920", max_width=1200, max_height=1200, store=True)
